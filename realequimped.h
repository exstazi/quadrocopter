#ifndef REALEQUIMPED_H
#define REALEQUIMPED_H

#include "service/icommunicate.h"
#include "iradioequimped.h"

class RealEquimped: public IRadioEquimped
{
    Q_OBJECT
private:
    ICommunicate *communicate;
public:
    RealEquimped( ICommunicate *communicate );
    ~RealEquimped();
private slots:
    void readData( uchar command,int value );
};

#endif // REALEQUIMPED_H
