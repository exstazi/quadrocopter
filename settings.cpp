#include "settings.h"
#include "ui_settings.h"

#include <QDebug>
#include "realequimped.h"
#include "realsensors.h"

Settings::Settings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);

    equimped = NULL;
    sensors = NULL;

    connect( &form_communicate,SIGNAL(createdCommunicate(ICommunicate*)),this,SLOT(createdCommnucate(ICommunicate*)) );
}

void Settings::createdCommnucate( ICommunicate *communicate )
{
    if( equimped == NULL )
    {
       equimped = new RealEquimped( communicate );
       connect( equimped,SIGNAL(changedStickGas(int)),ui->stick_l,SLOT(setY(int)) );
       connect( equimped,SIGNAL(changedStickYaw(int)),ui->stick_l,SLOT(setX(int)) );
       connect( equimped,SIGNAL(changedStickPitch(int)),ui->stick_r,SLOT(setY(int)) );
       connect( equimped,SIGNAL(changedStickRoll(int)),ui->stick_r,SLOT(setX(int)) );
    } else {
        sensors = new RealSensors( communicate );
    }

    qDebug() << communicate;
}

Settings::~Settings()
{
    delete ui;
}

void Settings::on_pushButton_clicked()
{
    form_communicate.show();
}
