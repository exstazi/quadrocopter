#ifndef MODELCOPTER_H
#define MODELCOPTER_H

class ModelCopter
{
protected:
    int front_l;
    int front_r;
    int rear_l;
    int rear_r;
public:
    virtual void print(float x,float y,float z,int roll,int pitch,int yaw) = 0;
    void setSpeed( int front_l,int front_r,int rear_l,int rear_r ) {
        this->front_l = front_l;
        this->front_r = front_r;
        this->rear_l = rear_l;
        this->rear_r = rear_r;
    }
};

#endif // MODELCOPTER_H
