#ifndef SETTINGS_H
#define SETTINGS_H

#include <QWidget>
#include "service/icommunicate.h"
#include "viewcommunicate.h"
#include "iradioequimped.h"
#include "imemssensors.h"
#include "realsensors.h"

namespace Ui {
class Settings;
}

class Settings : public QWidget
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = 0);
    ~Settings();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Settings *ui;
    ViewCommunicate form_communicate;

    IRadioEquimped *equimped;
    RealSensors *sensors;


private slots:
    void createdCommnucate( ICommunicate *communicate );
};

#endif // SETTINGS_H
