#include "dijkstra.h"
#include <QDebug>
#include <iostream>

using std::cout;
using std::endl;

Dijkstra::Dijkstra(VirtualWorld *world)
{
    this->world = world;
}

Dijkstra::~Dijkstra()
{

}

void Dijkstra::setObstacle(bool ***barrier)
{
    map = barrier;
    visited = new bool*[world->length];
    distance = new int*[world->length];
    for( uint i = 0; i < world->length; ++i )
    {
        visited[i] = new bool[world->width];
        distance[i] = new int[world->width];
        for( uint j = 0; j < world->width; ++j )
        {
            distance[i][j] = INT_MAX;
            visited[i][j] = false;
            cout << map[i][j][0] << " ";
        }
        cout << endl;
    }
    distance[0][0] = 0;
    visited[0][0] = false;

    uint l = world->length;
    uint w = world->width;

    int active = 0;
    for( uint i = 0; i < l; ++i )
    {
        for( uint j = 0; j < w; ++j )
        {
            if( !visited[i][j] && !map[i][j][0] )
            {
                ++active;
                distance[i][j] = active;
            }
        }
    }

    for( uint i = 0; i < l; ++i )
    {
        for( uint j = 0; j < w; ++j )
        {
            cout << distance[i][j] << " ";
        }
        cout << endl;
    }
}

