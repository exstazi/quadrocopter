#include "virtualsensors.h"
#include <QDebug>
#include <qmath.h>

VirtualSensors::VirtualSensors(Copter *copter, SceneWorld *scene)
{
    this->copter = copter;
    this->scene = scene;
    roll = pitch = yaw = 25;
    connect( copter,SIGNAL(changedPwm(int,int,int,int)),this,SLOT(changePwm(int,int,int,int)) );
    connect( &timer,SIGNAL(timeout()),this,SLOT(update()) );
    timer.start( 100 );
}

void VirtualSensors::changePwm( int front_l, int front_r, int rear_l, int rear_r )
{
    roll += ( 0.75 * 0.000014 * ( front_l*front_l + front_r*front_r - rear_l*rear_l - rear_r*rear_r ) ) / 0.011;
    pitch += ( 0.75 * 0.000014 * ( front_l*front_l - front_r*front_r + rear_l*rear_l - rear_r*rear_r ) ) / 0.011;
    yaw = ( 0.75 * 0.000014 * ( front_l*front_l - front_r*front_r - rear_l*rear_l + rear_r*rear_r ) ) / 0.011;
    altitude = ( altitude + yaw ) % 360;

    float Fn = ( front_l + front_r + rear_l + rear_r ) * 0.000014;
    float forward = Fn * pitch;
    float sideways = Fn* roll;
    float cos_yaw = cos( altitude * 0.0174532925 );
    float sin_yaw = sin( altitude * 0.0174532925 );

    x += forward * cos_yaw + sideways * sin_yaw;
    y += forward * sin_yaw - sideways * cos_yaw;
}

void VirtualSensors::update()
{
    float * barrier = scene->checkPosistion( x,y,0 );
    for( int i = 0; i < 4; ++i ) {
        if( barrier[i] < 0.1 ) {
            int angle = altitude>0?altitude:(360+altitude);
            if( angle > 45 && angle <= 135 ) {
                emit difficultyAhead( i );
            } else if( angle > 135  && angle <= 225 ) {
                emit difficultyAhead( (i+1)%4 );
            } else if( angle > 225 && angle <= 315 ) {
                emit difficultyAhead( (i+2)%4 );
            } else {
                emit difficultyAhead( (i+3)%4 );
            }
        }
    }
}

VirtualSensors::~VirtualSensors()
{
    delete copter;
}

