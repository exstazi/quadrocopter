#ifndef IRADIOEQUIMPED
#define IRADIOEQUIMPED

#include <QObject>

class IRadioEquimped: public QObject
{
    Q_OBJECT
signals:
    void changedStickGas( int value );
    void changedStickRoll( int value );
    void changedStickPitch( int value );
    void changedStickYaw( int value );
    void pressedButton( int number );
};


#endif // IRADIOEQUIMPED

