#include "virtualworld.h"
#include <QDebug>
#include <iostream>

VirtualWorld::VirtualWorld(QWidget *parent):
    SceneWorld(parent)
{
    this->length = 10;
    this->width = 10;
    this->height = 2;
    fps = 25;
    timer = new QTimer;
    connect( timer,SIGNAL(timeout()),this,SLOT(updateGL()) );
    timer->start( fps );
    generateWorld();
    //printWorld();
}

VirtualWorld::VirtualWorld( uint l,uint w,uint h,QWidget *parent ):
    SceneWorld(parent)
{
    this->length = l;
    this->width = w;
    this->height = h;
    fps = 25;
    timer = new QTimer;
    connect( timer,SIGNAL(timeout()),this,SLOT(updateGL()) );
    timer->start( fps );
    generateWorld();
    printWorld();
}

void VirtualWorld::generateWorld()
{
    world = new int**[length];
    for( uint i = 0; i < length; ++i ) {
        world[i] = new int*[width];
        for( uint j = 0; j < width; ++j ) {
            world[i][j] = new int[height];
            for( uint k = 0; k < height; ++k )
                world[i][j][k] = 0;
        }
    }
    /*for( uint i = 0; i < length; ++i ) {
        world[i][0][0] = 1;
        world[i][width-1][0] = 1;
    }

    for( uint i = 0; i < width; ++i ) {
        world[0][i][0] = 1;
        world[length-1][i][0] = 1;
    }*/
}

void VirtualWorld::printWorld()
{
    for( uint i = 0; i < length; ++i ) {
        for( uint j = 0; j < width; ++j ) {
            std::cout << world[i][j][0] << " ";
        }
        std::cout << std::endl;
    }
}

void VirtualWorld::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glScalef(scale, scale, scale);
    glTranslatef( offset_x, offset_y, offset_z );
    glRotatef(rotate_x, 1.0f, 0.0f, 0.0f);
    glRotatef(rotate_y, 0.0f, 1.0f, 0.0f);
    glRotatef(rotate_z, 0.0f, 0.0f, 1.0f);
    glTranslatef( -(int)(length/2), -(int)width/2, 0 );

    this->printGrid();



    glColor3f( 1,1,1 );
    glBegin(GL_LINES);
    for( uint i = 0; i < length; ++i )
    {
        for( uint j = 0; j < width; ++j )
        {
            for( uint k = 0; k < height; ++k )
            {
                if( world[i][j][k] == 1 )
                {
                    glVertex3f( i,  j,  (GLfloat)k/2);
                    glVertex3f( i+1,j,  (GLfloat)k/2);
                    glVertex3f( i+1,j+1, (GLfloat)k/2);
                    glVertex3f( i,  j+1, (GLfloat)k/2);

                    glVertex3f( i,  j,  (GLfloat)(k+1)/2);
                    glVertex3f( i+1,j,  (GLfloat)(k+1)/2);
                    glVertex3f( i+1,j+1, (GLfloat)(k+1)/2);
                    glVertex3f( i,  j+1, (GLfloat)(k+1)/2);

                    glVertex3f( i,  j,  (GLfloat)k/2);
                    glVertex3f( i+1,j,  (GLfloat)k/2);
                    glVertex3f( i+1,j, (GLfloat)(k+1)/2);
                    glVertex3f( i,  j, (GLfloat)(k+1)/2);

                    glVertex3f( i,  j+1,  (GLfloat)k/2);
                    glVertex3f( i+1,j+1, (GLfloat) k/2);
                    glVertex3f( i+1,j+1, (GLfloat)(k+1)/2);
                    glVertex3f( i,  j+1, (GLfloat)(k+1)/2);

                    glVertex3f( i,  j,  (GLfloat)k/2);
                    glVertex3f( i,  j+1, (GLfloat) k/2);
                    glVertex3f( i,  j+1, (GLfloat)(k+1)/2);
                    glVertex3f( i,  j, (GLfloat)(k+1)/2);

                    glVertex3f( i+1,  j,  (GLfloat)k/2);
                    glVertex3f( i+1,  j+1, (GLfloat) k/2);
                    glVertex3f( i+1,  j+1, (GLfloat)(k+1)/2);
                    glVertex3f( i+1,  j, (GLfloat)(k+1)/2);
                }
            }
        }
    }
    glEnd();

    glColor3f( 0,0,0.6 );
    glLineWidth(3.0f);
    glBegin(GL_QUADS);
    for( uint i = 0; i < length; ++i )
    {
        for( uint j = 0; j < width; ++j )
        {
            for( uint k = 0; k < height; ++k )
            {
                if( world[i][j][k] == 1 )
                {
                    glVertex3f( i,  j,  (GLfloat)k/2);
                    glVertex3f( i+1,j,  (GLfloat)k/2);
                    glVertex3f( i+1,j+1, (GLfloat)k/2);
                    glVertex3f( i,  j+1, (GLfloat)k/2);

                    glVertex3f( i,  j,  (GLfloat)(k+1)/2);
                    glVertex3f( i+1,j,  (GLfloat)(k+1)/2);
                    glVertex3f( i+1,j+1, (GLfloat)(k+1)/2);
                    glVertex3f( i,  j+1, (GLfloat)(k+1)/2);

                    glVertex3f( i,  j,  (GLfloat)k/2);
                    glVertex3f( i+1,j,  (GLfloat)k/2);
                    glVertex3f( i+1,j, (GLfloat)(k+1)/2);
                    glVertex3f( i,  j, (GLfloat)(k+1)/2);

                    glVertex3f( i,  j+1,  (GLfloat)k/2);
                    glVertex3f( i+1,j+1, (GLfloat) k/2);
                    glVertex3f( i+1,j+1, (GLfloat)(k+1)/2);
                    glVertex3f( i,  j+1, (GLfloat)(k+1)/2);

                    glVertex3f( i,  j,  (GLfloat)k/2);
                    glVertex3f( i,  j+1, (GLfloat) k/2);
                    glVertex3f( i,  j+1, (GLfloat)(k+1)/2);
                    glVertex3f( i,  j, (GLfloat)(k+1)/2);

                    glVertex3f( i+1,  j,  (GLfloat)k/2);
                    glVertex3f( i+1,  j+1, (GLfloat) k/2);
                    glVertex3f( i+1,  j+1, (GLfloat)(k+1)/2);
                    glVertex3f( i+1,  j, (GLfloat)(k+1)/2);
                }
            }
        }
    }
    glEnd();

    for( int i = 0; i < copters.length(); ++i ) {
        copters.at(i)->print();
    }

}

void VirtualWorld::setViewGrid( bool mode )
{
    mode_view_grid = mode;
}

void VirtualWorld::printGrid()
{
    glColor3f(1,1,1);
    glLineWidth(1.0f);
    glEnable(GL_LINE_SMOOTH);
    glLineStipple(1,255);
    glEnable(GL_LINE_STIPPLE);
    glBegin(GL_LINES);

    for( uint i = 0; i <= length; ++i ) {
        glVertex3f( i,  width,  0);
        glVertex3f( i,  width,  height/2);

        glVertex3f( i,  0.0f,  0);
        glVertex3f( i,  width, 0);
    }

    for( uint i = 0; i <= width; ++i ) {
        glVertex3f( 0,  i,  0);
        glVertex3f( length,  i, 0);

        glVertex3f( 0,  i,  0);
        glVertex3f( 0,  i,  height/2);
    }

    for( GLfloat i = 0.5; i <= height / 2 + 0.5; i+= 0.5 ) {
        glVertex3f( 0,  0,  i);
        glVertex3f( 0,  width, i);

        glVertex3f( 0,  width,  i);
        glVertex3f( length,  width, i);
    }

    glEnd();
}

void VirtualWorld::setViewPlane( bool mode )
{
    mode_view_plane = mode;
}

void VirtualWorld::printPlane()
{

}

void VirtualWorld::setObstacle(bool ***barrier)
{
    for( uint i = 0; i < length; ++i )
    {
        for( uint j = 0; j < width; ++j )
        {
            for( uint k = 0; k < height; ++k )
            {
                if( barrier[i][j][k] )
                {
                    world[i][j][k] = 1;
                } else {
                    world[i][j][k] = 0;
                }
            }
        }
    }
}

float* VirtualWorld::checkPosistion(float x, float y, float z)
{
    float *barrier = new float[4];
    barrier[0] = x - 0.5;
    barrier[1] = width - y - 0.5;
    barrier[2] = length - x - 0.5;
    barrier[3] = y - 0.5;
    return barrier;
}

void VirtualWorld::setSizeWorld(uint legnth, uint width, uint height)
{
    timer->stop();
    this->length = legnth;
    this->width = width;
    this->height = height;
    generateWorld();
    timer->start();
}

void VirtualWorld::setFps(uint fps)
{
    this->fps = 1000 / fps;
    timer->setInterval( this->fps );
}

VirtualWorld::~VirtualWorld()
{
    delete world;
}

