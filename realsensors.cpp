#include "realsensors.h"
#include <QDebug>

RealSensors::RealSensors(ICommunicate *communicate)
{
    this->communicate = communicate;
    connect( communicate,SIGNAL(readyData(uchar,int)),this,SLOT(readData(uchar,int)) );
    communicate->communicate();
}

void RealSensors::readData( uchar command, int value )
{
    qDebug() << command << " " << value;
}

void RealSensors::update()
{

}

RealSensors::~RealSensors()
{
    delete communicate;
}

