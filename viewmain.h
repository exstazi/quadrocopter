#ifndef VIEWMAIN_H
#define VIEWMAIN_H

#include <QWidget>
#include <QMouseEvent>
#include <QResizeEvent>
#include <QSerialPort>
#include <QSerialPortInfo>
#include "sceneworld.h"
#include "virtualworld.h"
#include "ui/spacemap.h"
#include "dijkstra.h"

namespace Ui {
class ViewMain;
}

class ViewMain : public QWidget
{
    Q_OBJECT
private:
    QString green_color;
    QString red_color;
    QString blue_color;

public:
    explicit ViewMain(QWidget *parent = 0);
    ~ViewMain();

protected:
    void mouseMoveEvent(QMouseEvent *event);
    void resizeEvent(QResizeEvent *);

private:
    Ui::ViewMain *ui;
    SceneWorld *scene;

    QSerialPort *port;

    VirtualWorld *world;
    SpaceMap *map;

    Dijkstra *alg;

private slots:
    void btn_clk();
    void changeToggle( int index,QString title,bool toggle );
    void nav_clk(int index);

    void btnAddCopter();
    void btnEditVirtualWolrd();
    void btnConnectToComPort();
    void btnSendDataToComPort();
    void btnClearTerminalComPort();

    void comPortDataReady();
};

#endif // VIEWMAIN_H
