#ifndef IMEMSSENSORS
#define IMEMSSENSORS

#include <QObject>
#include <QTimer>

class IMemsSensors: public QObject
{
    Q_OBJECT
protected:
    int roll;
    int pitch;
    int yaw;
    int altitude;
    float x;
    float y;

    float time_measurements;
    QTimer timer;
public:
    int getRoll() { return roll; }
    int getPitch() { return pitch; }
    int getYaw() { return yaw; }
    int getAltitude() { return altitude; }
    float getX() { return x; }
    float getY() { return y; }
signals:
    void difficultyAhead( int where );
private slots:
    virtual void update() = 0;
};

#endif // IMEMSSENSORS

