#ifndef KEYBORDEQUIMPED_H
#define KEYBORDEQUIMPED_H

#include <QWidget>
#include <QMainWindow>
#include "iradioequimped.h"

class KeybordEquimped: public IRadioEquimped
{
    Q_OBJECT
protected:
    int roll;
    int pitch;
    int yaw;
    int gas;
public:
    KeybordEquimped();
public slots:
    void keyPressEvent( int key );
};

#endif // KEYBORDEQUIMPED_H
