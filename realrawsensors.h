#ifndef REALRAWSENSORS_H
#define REALRAWSENSORS_H

#include "imemssensors.h"

class RealRawSensors: public IMemsSensors
{
public:
    RealRawSensors();
    ~RealRawSensors();
};

#endif // REALRAWSENSORS_H
