#ifndef VIEWCOMMUNICATE_H
#define VIEWCOMMUNICATE_H

#include <QWidget>
#include "service/icommunicate.h"
#include "service/comport.h"

namespace Ui {
class ViewCommunicate;
}

class ViewCommunicate : public QWidget
{
    Q_OBJECT

public:
    explicit ViewCommunicate(QWidget *parent = 0);
    ~ViewCommunicate();

private:
    Ui::ViewCommunicate *ui;
    ComPort *port;

signals:
    void createdCommunicate( ICommunicate *communicate );
private slots:
    void updateListComPort();
    void connectToComPort();
};

#endif // VIEWCOMMUNICATE_H
