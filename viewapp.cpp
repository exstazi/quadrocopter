#include "viewapp.h"
#include "ui_viewapp.h"
#include "service/comport.h"
#include <QtSerialPort/QSerialPortInfo>
#include <QDebug>
#include <QResizeEvent>

#include "modelquadrocopter.h"
#include "virtualworld.h"
#include "virtualsensors.h"
#include "keybordequimped.h"

#include <QtNetwork/QNetworkConfiguration>
#include <QtNetwork/QNetworkConfigurationManager>

ViewApp::ViewApp(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ViewApp)
{
    ui->setupUi(this);
    QList<QSerialPortInfo> list = QSerialPortInfo::availablePorts();
    if( list.size() > 0 ) {
        a = new RealEquimped( new ComPort(list.at(0).portName()) );
        qDebug() << "123";
    } else {
        KeybordEquimped *key_equimped = new KeybordEquimped;
        a = key_equimped;
        connect( this,SIGNAL(keyPress(int)),key_equimped,SLOT(keyPressEvent(int)) );
    }
    scene = new VirtualWorld( 10,10,3,this );
    Copter *b = new Copter( a );
    VirtualSensors *c = new VirtualSensors( b,scene );
    b->setSensors( c );
    connect( b,SIGNAL(changedRoll(int)),ui->s_pos,SLOT(changeRoll(int)) );
    connect( b,SIGNAL(changedPitch(int)),ui->s_pos,SLOT(changePitch(int)) );
    connect( b,SIGNAL(changedPwm(int,int,int,int)),this,SLOT(changedPwm(int,int,int,int)) );
    scene->addCopter( b );
    ui->scene->insertWidget( 0, scene );

    connect( a,SIGNAL(changedStickYaw(int)),ui->stick_l,SLOT(setX(int)) );
    connect( a,SIGNAL(changedStickGas(int)),ui->stick_l,SLOT(setY(int)) );
    connect( a,SIGNAL(changedStickRoll(int)),ui->stick_r,SLOT(setX(int)) );
    connect( a,SIGNAL(changedStickPitch(int)),ui->stick_r,SLOT(setY(int)) );

    ui->logo->setPixmap( QPixmap( ":img/logo" ) );
    ui->btn->setDefault( true );

    connect( ui->btn,SIGNAL(clicked()),this,SLOT(btn_nav_click()) );
    connect( ui->btn2,SIGNAL(clicked()),this,SLOT(btn_nav_click()) );
    connect( ui->btn3,SIGNAL(clicked()),this,SLOT(btn_nav_click()) );
    connect( ui->btn4,SIGNAL(clicked()),this,SLOT(btn_nav_click()) );
    connect( ui->btn5,SIGNAL(clicked()),this,SLOT(btn_nav_click()) );
    connect( ui->btn6,SIGNAL(clicked()),this,SLOT(btn_nav_click()) );

    connect( ui->connect_wifi_socket,SIGNAL(clicked()),this,SLOT(connect_wifi()) );
    connect( ui->connect_wifi_network,SIGNAL(clicked()),this,SLOT(connect_wifi()) );
    connect( ui->connect_com_port,SIGNAL(clicked()),this,SLOT(connect_com_port()) );
}

void ViewApp::btn_nav_click()
{
    if( sender()->objectName() == "btn" )
    {
        ui->stacked->setCurrentIndex( 0 );
        ui->title_text->setText( "Отображение квадрокоптера" );
    }
    else if ( sender()->objectName() == "btn2" )
    {
        ui->stacked->setCurrentIndex( 1 );
        ui->title_text->setText( "Добавление квадрокоптера" );
    }
    else if( sender()->objectName() == "btn5" )
    {
        ui->stacked->setCurrentIndex( 2 );
        ui->title_text->setText( "Настройки" );
    }
    else if( sender()->objectName() == "btn6" )
    {
        ui->stacked->setCurrentIndex( 3 );
        ui->title_text->setText( "Виртуальный мир" );
    }
    else if( sender()->objectName() == "btn3" )
    {
        update_list_wifi_networks();
        ui->stacked->setCurrentIndex( 4 );
        ui->title_text->setText( "WiFi" );
    }
    else if( sender()->objectName() == "btn4" )
    {
        update_list_com_ports();
        ui->stacked->setCurrentIndex( 5 );
        ui->title_text->setText( "COM порт" );
    }
}

void ViewApp::connect_wifi()
{
    if( sender()->objectName() == "connect_wifi_socket" )
    {
        qDebug() << "alaa";
    }
    else if( sender()->objectName() == "connect_wifi_network" )
    {

    }
}

void ViewApp::connect_com_port()
{
    if( ui->list_com_port->count() != 0 )
    {
        ui->terminal_com_ports->appendPlainText( "Подключение к " + ui->list_com_port->currentText() + " ..." );
        port = new QSerialPort( ui->list_com_port->currentText() );
        if( port->open( QIODevice::ReadWrite ) )
        {
            if ( port->setBaudRate( ui->list_baud_rate->currentText().toInt() )
                    && port->setDataBits( (QSerialPort::DataBits)ui->list_data_bits->currentText().toInt() )
                    && port->setParity( (QSerialPort::Parity)ui->list_parity->currentText().toInt() )
                    && port->setStopBits( (QSerialPort::StopBits)ui->list_stop_bits->currentText().toInt() )
                    && port->setFlowControl( (QSerialPort::FlowControl)ui->list_flow_control->currentText().toInt() ) )
            {
                if ( port->isOpen() ){
                    ui->terminal_com_ports->appendPlainText( "Открыто соединение с " + ui->list_com_port->currentText() );
                    return ;
                }
            }
        }
    }
    ui->terminal_com_ports->appendPlainText( port->errorString() );
}

void ViewApp::update_list_wifi_networks()
{
    ui->list_wifi_networks->clear();
    QNetworkConfigurationManager ncm;
    QList<QNetworkConfiguration> nc = ncm.allConfigurations();
    for (int i = 0; i < nc.size(); ++i )
    {
        if (nc.at(i).bearerType() == QNetworkConfiguration::BearerWLAN)
        {
            ui->list_wifi_networks->addItem( nc.at(i).name() );
            //qDebug() << nc.at(i).name() << " " << nc.at(i).identifier() << " " << nc.at(i).identifier();
        }
    }

}

void ViewApp::update_list_com_ports()
{
    ui->list_com_port->clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        if( !info.isBusy() )
        ui->list_com_port->addItem(info.portName());
    }
}

void ViewApp::keyPressEvent(QKeyEvent *event)
{
    emit keyPress( event->nativeVirtualKey() );
}

void ViewApp::changedPwm(int a, int b, int c, int d)
{
    ui->front_l->setValue( a );
    ui->front_r->setValue( b );
    ui->rear_l->setValue( c );
    ui->rear_r->setValue( d );
}

void ViewApp::resizeEvent(QResizeEvent *event)
{
    ui->horizontalLayoutWidget->resize( event->size() );
    ui->verticalLayoutWidget->resize( event->size().width()-ui->left_column->width(),
                                      event->size().height()-ui->title->height() );
    ui->stick_l->setGeometry( 0, event->size().height() - 140,81,81 );
    ui->stick_r->setGeometry( event->size().width()-293, event->size().height() - 140,81,81 );
}

ViewApp::~ViewApp()
{
    delete ui;
}

void ViewApp::on_tabWidget_tabBarClicked(int index)
{
    qDebug() << index;
}
