#ifndef REALSENSORS_H
#define REALSENSORS_H

#include <QObject>
#include "service/icommunicate.h"
#include "imemssensors.h"

class RealSensors: public IMemsSensors
{
    Q_OBJECT
private:
    ICommunicate *communicate;
public:
    RealSensors( ICommunicate *communicate );
    ~RealSensors();
private slots:
    void readData( uchar command,int value );
    void update();
};

#endif // REALSENSORS_H
