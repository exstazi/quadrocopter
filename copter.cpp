#include "copter.h"
#include "modelquadrocopter.h"
#include "virtualsensors.h"
#include "viewapp.h"
#include <QDebug>
#include <qmath.h>

Copter::Copter(IRadioEquimped *equimped )
{
    this->equimped = equimped;
    this->model = new ModelQuadroCopter();

    x = y = z = 5;
    z = 0;
    roll = pitch = yaw = gas = 0;
    front_l = front_r = rear_l = rear_r = 0;
    emit changedPwm( front_l,front_r,rear_l,rear_r );
    pid_roll.setTask(0);
    pid_pitch.setTask(0);
    pid_yaw.setTask(0);

    changeGas( 12 );

    connect( equimped,SIGNAL(changedStickGas(int)),this,SLOT(changeGas(int)) );
    connect( equimped,SIGNAL(changedStickRoll(int)),this,SLOT(changeRoll(int)) );
    connect( equimped,SIGNAL(changedStickPitch(int)),this,SLOT(changePitch(int)) );
    connect( equimped,SIGNAL(changedStickYaw(int)),this,SLOT(changeYaw(int)) );

    connect( &timer,SIGNAL(timeout()),this,SLOT(update()) );
    timer.start(25);
}

void Copter::update()
{
    int force = pid_roll.getForce( sensors->getRoll() );
    front_l += force;
    front_r += force;
    rear_l -= force;
    rear_r -= force;
    force = pid_pitch.getForce( sensors->getPitch() );
    front_l += force;
    rear_l += force;
    front_r -= force;
    rear_r -= force;
    force = pid_yaw.getForce( sensors->getYaw() );
    front_l += force;
    front_r -= force;
    rear_l -= force;
    rear_r += force;

    emit changedPwm( front_l,front_r,rear_l,rear_r );
    emit changedRoll( sensors->getRoll() );
    emit changedPitch( sensors->getPitch() );
}

void Copter::processingBarrier(int where)
{
    /*switch( where ) {
    case 0:
        pid_roll.setTask( 0 );
        //qDebug() << "left " << sensors->getAltitude();
        break;
    case 1:
        pid_pitch.setTask( 0 );
        //qDebug() << "ahead " << sensors->getAltitude();
        break;
    case 2:
        pid_roll.setTask( 0 );
        //qDebug() << "rigth " << sensors->getAltitude();
        break;
    case 3:
        pid_pitch.setTask( 0 );
        //qDebug() << "back " << sensors->getAltitude();
    }*/
}

void Copter::changeGas(int value)
{
    value >>= 1;
    value *= 8;
    front_l += value - gas;
    front_r += value - gas;
    rear_l += value - gas;
    rear_r += value - gas;
    gas = value;
}

void Copter::changeRoll(int value)
{
    roll = value;//(256-value)/12.8;
    pid_roll.setTask( roll );
}

void Copter::changePitch(int value)
{
    pitch = value;//(value-256)/12.8;
    pid_pitch.setTask( pitch );
}

void Copter::changeYaw(int value)
{
    yaw = value; //(value-256)/12.8;
    pid_yaw.setTask( yaw );
}

void Copter::setModel(ModelCopter *model)
{
    if( model != NULL ) {
        this->model = model;
    }
}

void Copter::setSensors(IMemsSensors *sensors)
{
    this->sensors = sensors;
    connect( sensors,SIGNAL(difficultyAhead(int)),this,SLOT(processingBarrier(int)) );
}

void Copter::print()
{
    model->setSpeed( front_l,front_r,rear_l,rear_r );
    model->print(x,y,z,0,0,sensors->getAltitude() );
    //model->print(sensors->getX(),sensors->getY(),z,0,0,sensors->getAltitude() );
}

Copter::~Copter()
{
    delete equimped;
    delete sensors;
    //delete model;
}

