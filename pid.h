#ifndef PID_H
#define PID_H

class Pid
{
private:
    double P;
    double I;
    double D;

    double integral;
    double lastError;

    double dt;

    double task;

public:
    Pid();
    Pid( double p, double i, double d );
    void setTask( double task );
    double getTask();
    void setParam( double p,double i,double d );
    double getForce( double angle );
    bool operator == (const Pid two );
    void reset();
};

#endif
