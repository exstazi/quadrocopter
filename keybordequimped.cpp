#include "keybordequimped.h"
#include <QDebug>

KeybordEquimped::KeybordEquimped()
{
    roll = pitch = yaw = 255;
    gas = 100;
}

void KeybordEquimped::keyPressEvent(int key)
{
    switch( key )
    {
    case Qt::Key_W:
        pitch += 20;
        emit changedStickPitch( pitch );
        break;
    case Qt::Key_S:
        pitch -= 20;
        emit changedStickPitch( pitch );
        break;
    case Qt::Key_Q:
        yaw += 20;
        emit changedStickYaw( yaw );
        break;
    case Qt::Key_E:
        yaw -= 20;
        emit changedStickYaw( yaw );
        break;
    case Qt::Key_A:
        roll += 20;
        emit changedStickRoll( roll );
        break;
    case Qt::Key_D:
        roll -= 20;
        emit changedStickRoll( roll );
        break;
    case Qt::Key_R:
        gas += 20;
        emit changedStickGas( gas );
        break;
    case Qt::Key_F:
        gas -= 20;
        emit changedStickGas( gas );
        break;
    }
}

