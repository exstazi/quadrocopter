#include "viewcommunicate.h"
#include "ui_viewcommunicate.h"
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QList>
#include <QDebug>
#include "service/comport.h"

ViewCommunicate::ViewCommunicate(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ViewCommunicate)
{
    ui->setupUi(this);

    updateListComPort();

    ui->BaudRateBox->addItem(QLatin1String("9600"), QSerialPort::Baud9600);
    ui->BaudRateBox->addItem(QLatin1String("19200"), QSerialPort::Baud19200);
    ui->BaudRateBox->addItem(QLatin1String("38400"), QSerialPort::Baud38400);
    ui->BaudRateBox->addItem(QLatin1String("115200"), QSerialPort::Baud115200);
    ui->BaudRateBox->addItem(QLatin1String("Custom"));
    // fill data bits
    ui->DataBitsBox->addItem(QLatin1String("5"), QSerialPort::Data5);
    ui->DataBitsBox->addItem(QLatin1String("6"), QSerialPort::Data6);
    ui->DataBitsBox->addItem(QLatin1String("7"), QSerialPort::Data7);
    ui->DataBitsBox->addItem(QLatin1String("8"), QSerialPort::Data8);
    ui->DataBitsBox->setCurrentIndex(3);
    // fill parity
    ui->ParityBox->addItem(QLatin1String("None"), QSerialPort::NoParity);
    ui->ParityBox->addItem(QLatin1String("Even"), QSerialPort::EvenParity);
    ui->ParityBox->addItem(QLatin1String("Odd"), QSerialPort::OddParity);
    ui->ParityBox->addItem(QLatin1String("Mark"), QSerialPort::MarkParity);
    ui->ParityBox->addItem(QLatin1String("Space"), QSerialPort::SpaceParity);
    // fill stop bits
    ui->StopBitsBox->addItem(QLatin1String("1"), QSerialPort::OneStop);
    #ifdef Q_OS_WIN
    ui->StopBitsBox->addItem(QLatin1String("1.5"), QSerialPort::OneAndHalfStop);
    #endif
    ui->StopBitsBox->addItem(QLatin1String("2"), QSerialPort::TwoStop);
    // fill flow control
    ui->FlowControlBox->addItem(QLatin1String("None"), QSerialPort::NoFlowControl);
    ui->FlowControlBox->addItem(QLatin1String("RTS/CTS"), QSerialPort::HardwareControl);
    ui->FlowControlBox->addItem(QLatin1String("XON/XOFF"), QSerialPort::SoftwareControl);

    connect( ui->Btn_Serch,SIGNAL(clicked()),this,SLOT(updateListComPort()) );
    connect( ui->connect,SIGNAL(clicked()),this,SLOT(connectToComPort()) );
}

void ViewCommunicate::updateListComPort()
{
    ui->PortNameBox->clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        if( !info.isBusy() )
        ui->PortNameBox->addItem(info.portName());
    }
}

void ViewCommunicate::connectToComPort()
{
    port = new ComPort( ui->PortNameBox->currentText(),
                                 ui->BaudRateBox->currentText().toInt(),
                                 ui->DataBitsBox->currentText().toInt(),
                                 ui->ParityBox->currentText().toInt(),
                                 ui->StopBitsBox->currentText().toInt(),
                                 ui->FlowControlBox->currentText().toInt() );
    if( port->communicate() )
    {
        emit createdCommunicate( port );
        updateListComPort();
        this->close();
    }
}

ViewCommunicate::~ViewCommunicate()
{
    delete ui;
}
