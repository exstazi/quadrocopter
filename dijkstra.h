#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include <QObject>
#include "iradioequimped.h"
#include "virtualworld.h"



class Dijkstra : public IRadioEquimped
{
    Q_OBJECT
private:
    VirtualWorld *world;
    bool ***map;

    bool **visited;
    int **distance;

public:
    Dijkstra(VirtualWorld *world );
    ~Dijkstra();

    void setObstacle( bool ***barrier );

};

#endif // DIJKSTRA_H
