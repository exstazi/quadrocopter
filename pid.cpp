#include "pid.h"

Pid::Pid() {
    setParam( 0.2,0.01,0.1 );
}

Pid::Pid( double p, double i, double d ) {
    setParam( p,i,d );
}

void Pid::setTask( double task ){
    if( this->task == task )
        return ;
    reset();
    this->task = task;
}

double Pid::getTask() {
    return task;
}

void Pid::setParam(double p, double i, double d) {
    P = p;
    I = i;
    D = d;
    task = 0;
    reset();
}

double Pid::getForce( double angle ) {
    double error = task - angle;
    integral += error * I;
    double derivative = ( error - lastError) / dt;
    lastError = error;
    return P * error  +  integral * dt  +  D * derivative;
}

bool Pid::operator ==(const Pid two ) {
    return (this->P == two.P && this->I == two.I && this->D == two.D);
}

void Pid::reset() {
    integral = lastError = 0.0;
    dt = 0.2;
}
