#ifndef MODELQUADROCOPTER_H
#define MODELQUADROCOPTER_H

#include "modelcopter.h"
#include "service/objreader.h"

class ModelQuadroCopter: public ModelCopter
{
private:
    CVector3<float> * body;
    CVector3<float> * propeller;
    int size_body;
    int size_propeller;
    void createArray();

    int speed[4];
protected:
    ObjReader * obj_model;
public:
    ModelQuadroCopter();
    ~ModelQuadroCopter();

    void print(float x,float y,float z,int roll,int pitch,int yaw);
};

#endif // MODELQUADROCOPTER_H
