#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H

#include <QWidget>
#include <QPainter>

class ProgressBar : public QWidget
{
    Q_OBJECT
public:
    explicit ProgressBar(QWidget *parent = 0);
    void setMax( int max );
    void setValue( int value );
    void setSignature( QString text );

protected:
    int maxValue;
    int currValue;
    QString signature;

    void paintEvent(QPaintEvent *);

};



#endif // PROGRESSBAR_H
