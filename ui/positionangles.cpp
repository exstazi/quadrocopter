#include "positionangles.h"
#include <math.h>
#include <qmath.h>
#include <QDebug>

PositionAngles::PositionAngles(QWidget *parent) :
    QWidget(parent)
{
    angle = 0;
}

void PositionAngles::setAngle(int angle) {
    this->angle = angle % 360;
    this->update();
}

void PositionAngles::setText( QString text ) {
    this->text = text;
    this->update();
}

void PositionAngles::paintEvent(QPaintEvent *) {
    QPainter p(this);
    p.setPen( QPen(Qt::gray,1,Qt::SolidLine) );
    p.setBrush( Qt::gray );
    p.drawEllipse( QPoint(30,30),30,30 );
    p.setPen( QPen(Qt::blue,3,Qt::SolidLine) );
    int x = 25 * cos( 0.0174532925 * angle);
    int y = 25 * sin( 0.0174532925 * angle );
    p.drawLine( QPoint( 30 + x, 30 - y ),QPoint( 30 - x,30 + y ) );

    y = 10 * cos( 0.0174532925 * (-angle) );
    x = 10 * sin( 0.0174532925 * (-angle) );
    p.drawLine( QPoint(30,30),QPoint( 30 + x, 30 - y ) );
    p.drawText(60,0, 150,20,1, QString::number( angle ) );
    p.setPen( Qt::black );
    p.drawText(QPoint(20,50),text);
}


Compass::Compass(QWidget *parent)
    :PositionAngles(parent)
{
    angle = 0;
}

void Compass::paintEvent(QPaintEvent *) {
    QPainter p(this);
    p.setPen( QPen(Qt::gray,1,Qt::SolidLine) );
    p.setBrush( Qt::gray );
    p.drawEllipse( QPoint(60,60),60,60 );
    p.setBrush( Qt::blue );
    p.drawEllipse( QPoint(60,60),40,40 );
    p.setPen( Qt::black );
    p.drawText( QPoint( 58,15),"N" );
    p.drawText( QPoint( 58,115),"S" );
    p.drawText( QPoint( 5,60),"W" );
    p.drawText( QPoint( 110,60),"E" );
    p.setPen( QPen(Qt::white,3,Qt::SolidLine));
    int x = 39 * sin( 0.0174532925 * (angle));
    int y = 39 * cos( 0.0174532925 * (angle) );
    p.drawLine( QPoint(60,60), QPoint(60+x,60-y) );
    p.setPen( Qt::blue );
    p.drawText(110,0, 150,20,1, QString::number( angle ) );
}
