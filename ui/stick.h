#ifndef STICK_H
#define STICK_H

#include <QWidget>
#include <QPainter>

class Stick : public QWidget
{
    Q_OBJECT
public:
    explicit Stick(QWidget *parent = 0);
    ~Stick();
public slots:
    void setX( int value );
    void setY( int value );
protected:
    int pos_x;
    int pos_y;
    void paintEvent(QPaintEvent *);
};

#endif // STICK_H
