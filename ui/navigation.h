#ifndef NAVIGATION_H
#define NAVIGATION_H

#include <QWidget>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QTimer>
#include <QPixmap>
#include <QEvent>

#include "hint.h"

class Navigation : public QWidget
{
    Q_OBJECT
protected:

    struct Button
    {
        QString name;
        QString icon;

        Button( QString n,QString i)
        {
            name = n;
            icon = i;
        }
        QString get( bool flag ) const
        {
            if( flag )
                return name;
            return icon;
        }
    };

    QColor color_text;
    QColor color_active_text;
    QColor color_block;
    QColor color_active_block;
    QColor color_left_rect;

    QPixmap *logo;

    int active_index;
    int select_index;

    bool is_full;

    QTimer *timer;

    QList<Button> list_button;

    int width_minimaze;
    int width_maximaze;
    int height_button;

    int speed_scroll;

    Hint *tool_tip;

public:
    explicit Navigation(QWidget *parent = 0);
    ~Navigation();

    void addButton(QString name , QString icon);
    void setMinimaze();
    void setMaximaze();
    void setHint( Hint* h ) { tool_tip = h; }

protected:
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void leaveEvent(QEvent*);
    void paintEvent(QPaintEvent *);

private slots:
    void updates();
signals:
    void clicked( int index );
    void clicked( int index,QString title );
};

#endif // NAVIGATION_H
