#ifndef SPATIALPOSITION_H
#define SPATIALPOSITION_H

#include <QWidget>
#include <QPainter>
#include <QTimer>

class SpatialPosition : public QWidget
{
    Q_OBJECT
public:
    explicit SpatialPosition(QWidget *parent = 0);
    ~SpatialPosition();
    QTimer *timer;
private:
    int roll;
    int pitch;
public slots:
    void changeRoll(int value);
    void changePitch( int value );
    void aa();
protected:
    void paintEvent(QPaintEvent *);
};

#endif // SPATIALPOSITION_H
