#include "tooltips.h"
#include <QDebug>
#include <QThread>

ToolTips::ToolTips(QWidget *parent) : QWidget(parent)
{
    timer = new QTimer;
    connect( timer,SIGNAL(timeout()),this,SLOT(updates()) );

    alpha = 255;

    setVisible( false );


    setWindowOpacity( 0.0 );


}

void ToolTips::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setPen( Qt::NoPen );
    p.setBrush((QBrush(QColor(44,56,69,alpha))));
    p.drawRoundRect(0,0,width(),height());
}

void ToolTips::show(QString text)
{
    this->text = text;
    alpha = 255;
    setVisible( true );
    repaint();
    QThread::sleep( 5 );
    timer->start( 100 );
}

void ToolTips::updates()
{
    alpha -= 5;
    if( alpha > 0 )
    {
        repaint();
    } else {
        timer->stop();
        setVisible( false );
    }
    qDebug() << alpha;
}

void ToolTips::leaveEvent(QEvent *)
{
    if( alpha >= 0 )
    {
        alpha = 255;
        timer->start( 100 );
    }
}

void ToolTips::enterEvent(QEvent *p)
{
    if( alpha >= 0 )
    {
    alpha = 255;
    repaint();
    timer->stop();
    }
}

ToolTips::~ToolTips()
{

}

