#ifndef TRAVELEDPANEL_H
#define TRAVELEDPANEL_H

#include <QWidget>
#include <QTimer>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QVector>
#include <QEvent>

class TraveledPanel : public QWidget
{
    Q_OBJECT

protected:

    struct Node
    {
        QString title;
        bool toogle;
        QString description;

        Node()
        {

        }

        Node( QString t,bool to,QString d )
        {
            title = t;
            toogle = to;
            description = d;
        }
        void set( bool t )
        {
            toogle = t;
        }
    };

    QColor color_block;
    QColor color_text;

    QTimer *timer;

    bool is_deployed;

    QVector<Node> list_settings;

public:
    explicit TraveledPanel(QWidget *parent = 0);
    ~TraveledPanel();

    void turn();
    void expand();
    bool isDeployed() { return is_deployed; }
    void addSetting( QString title,QString description,bool toogle = false );

protected:
    void paintEvent(QPaintEvent*);
    void mousePressEvent(QMouseEvent *event);
    void enterEvent(QEvent*);
    void leaveEvent(QEvent*);

private slots:
    void updates();

signals:
    void changeState( int index,QString text,bool toggle );
public slots:
};

#endif // TRAVELEDPANEL_H
