#ifndef HINT_H
#define HINT_H

#include <QWidget>
#include <QPaintEvent>
#include <QPainter>

class Hint : public QWidget
{
    Q_OBJECT

protected:
    QString text;
    QColor color;
public:
    explicit Hint(QWidget *parent = 0);
    ~Hint();

    void setText( QString t ) { text = t; }
    void setColor( QColor c ) { color = c; repaint(); }

protected:
    void paintEvent(QPaintEvent *);
};

#endif // HINT_H
