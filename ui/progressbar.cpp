#include "progressbar.h"
#include <QDebug>


ProgressBar::ProgressBar(QWidget *parent) :
    QWidget(parent)
{
    maxValue = 255;
    currValue = 20;
}

void ProgressBar::setMax(int max) {
    maxValue = max < 0?0:max;
    this->update();
}

void ProgressBar::setValue(int value) {
    currValue = value < 0?0:value > maxValue?maxValue:value;
    this->update();
}

void ProgressBar::setSignature( QString text ) {
    this->signature = text;
    this->update();
}

void ProgressBar::paintEvent(QPaintEvent *) {
    QPainter p(this);
    p.setPen( QPen(Qt::gray,1,Qt::SolidLine) );
    p.drawRect(0,0,15,60);
    p.setBrush( QBrush(QColor(0,128,192)) );
    int length = (60 * currValue) / 255;
    p.drawRect(0,60 - length,15,length);
    p.setPen( QColor( 0,0,0 ));
    p.drawText(25,46,110,20,1,QString::number( currValue ) );
    p.drawText(0,64,140,20,1,this->signature );
}


