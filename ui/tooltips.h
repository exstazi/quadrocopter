#ifndef TOOLTIPS_H
#define TOOLTIPS_H

#include <QWidget>
#include <QPainter>
#include <QPaintEvent>
#include <QTimer>
#include <QEvent>
#include <QThread>

class ToolTips : public QWidget
{
    Q_OBJECT
protected:
    QTimer * timer;
    int alpha;

    QString text;
    QThread *thread;

public:
    explicit ToolTips(QWidget *parent = 0);
    ~ToolTips();

    void show( QString text );


protected:
    void paintEvent(QPaintEvent *);
    void leaveEvent(QEvent*);
    void enterEvent(QEvent*p);

protected slots:
    void updates();

signals:
    void finished();
    void error(QString err);

};

#endif // TOOLTIPS_H
