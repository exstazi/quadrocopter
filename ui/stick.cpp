#include "stick.h"
#include "math.h"
#include <QDebug>

Stick::Stick(QWidget *parent) : QWidget(parent)
{
    pos_x = pos_y = 40;
}

void Stick::setX( int value ) {
    pos_x = 65 - (value / 10.24 );
    this->update();
}

void Stick::setY( int value ) {
    pos_y = 65 - (value / 10.24 );
    this->update();
}

void Stick::paintEvent(QPaintEvent *) {
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setPen( QPen(Qt::gray,2,Qt::SolidLine) );
    p.setBrush( Qt::gray );
    p.drawRect(0,0,100,100);
    p.setPen( QPen(Qt::black,1,Qt::SolidLine) );
    p.setBrush( Qt::white );
    p.drawRoundRect(0,0,80,80,40,40);
    p.setBrush( Qt::red );
    p.drawEllipse( QPoint(pos_x,pos_y),15,15 );
}

Stick::~Stick()
{

}

