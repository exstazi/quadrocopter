#ifndef FONTAWESOMEICONNAMES_H
#define FONTAWESOMEICONNAMES_H

//Qt
#include <QString>

class FontAwesomeIconNames
{
public:
    FontAwesomeIconNames() {}
    static QString unicode( QString iconName )
    {
        if ( iconName == "fa-hand-o-down" ) {
            return QString::fromUtf8( "\uf0a7" );
        }
        if ( iconName == "fa-circle-o-notch" ) {
            return QString::fromUtf8( "\uf1ce" );
        }
        if ( iconName == "fa-linkedin" ) {
            return QString::fromUtf8( "\uf0e1" );
        }
        if ( iconName == "fa-file-photo-o" ) {
            return QString::fromUtf8( "\uf1c5" );
        }
        if ( iconName == "fa-ellipsis-h" ) {
            return QString::fromUtf8( "\uf141" );
        }
        if ( iconName == "fa-qq" ) {
            return QString::fromUtf8( "\uf1d6" );
        }
        if ( iconName == "fa-simplybuilt" ) {
            return QString::fromUtf8( "\uf215" );
        }
        if ( iconName == "fa-laptop" ) {
            return QString::fromUtf8( "\uf109" );
        }
        if ( iconName == "fa-check-square-o" ) {
            return QString::fromUtf8( "\uf046" );
        }
        if ( iconName == "fa-plus-square" ) {
            return QString::fromUtf8( "\uf0fe" );
        }
        if ( iconName == "fa-phone-square" ) {
            return QString::fromUtf8( "\uf098" );
        }
        if ( iconName == "fa-exclamation-circle" ) {
            return QString::fromUtf8( "\uf06a" );
        }
        if ( iconName == "fa-university" ) {
            return QString::fromUtf8( "\uf19c" );
        }
        if ( iconName == "fa-flag" ) {
            return QString::fromUtf8( "\uf024" );
        }
        if ( iconName == "fa-calculator" ) {
            return QString::fromUtf8( "\uf1ec" );
        }
        if ( iconName == "fa-ellipsis-v" ) {
            return QString::fromUtf8( "\uf142" );
        }
        if ( iconName == "fa-angle-double-right" ) {
            return QString::fromUtf8( "\uf101" );
        }
        if ( iconName == "fa-exclamation" ) {
            return QString::fromUtf8( "\uf12a" );
        }
        if ( iconName == "fa-ra" ) {
            return QString::fromUtf8( "\uf1d0" );
        }
        if ( iconName == "fa-institution" ) {
            return QString::fromUtf8( "\uf19c" );
        }
        if ( iconName == "fa-paw" ) {
            return QString::fromUtf8( "\uf1b0" );
        }
        if ( iconName == "fa-euro" ) {
            return QString::fromUtf8( "\uf153" );
        }
        if ( iconName == "fa-list-ul" ) {
            return QString::fromUtf8( "\uf0ca" );
        }
        if ( iconName == "fa-save" ) {
            return QString::fromUtf8( "\uf0c7" );
        }
        if ( iconName == "fa-envelope-o" ) {
            return QString::fromUtf8( "\uf003" );
        }
        if ( iconName == "fa-pencil-square-o" ) {
            return QString::fromUtf8( "\uf044" );
        }
        if ( iconName == "fa-gamepad" ) {
            return QString::fromUtf8( "\uf11b" );
        }
        if ( iconName == "fa-beer" ) {
            return QString::fromUtf8( "\uf0fc" );
        }
        if ( iconName == "fa-cc-amex" ) {
            return QString::fromUtf8( "\uf1f3" );
        }
        if ( iconName == "fa-picture-o" ) {
            return QString::fromUtf8( "\uf03e" );
        }
        if ( iconName == "fa-medkit" ) {
            return QString::fromUtf8( "\uf0fa" );
        }
        if ( iconName == "fa-chevron-left" ) {
            return QString::fromUtf8( "\uf053" );
        }
        if ( iconName == "fa-indent" ) {
            return QString::fromUtf8( "\uf03c" );
        }
        if ( iconName == "fa-arrow-right" ) {
            return QString::fromUtf8( "\uf061" );
        }
        if ( iconName == "fa-info-circle" ) {
            return QString::fromUtf8( "\uf05a" );
        }
        if ( iconName == "fa-header" ) {
            return QString::fromUtf8( "\uf1dc" );
        }
        if ( iconName == "fa-folder-open-o" ) {
            return QString::fromUtf8( "\uf115" );
        }
        if ( iconName == "fa-briefcase" ) {
            return QString::fromUtf8( "\uf0b1" );
        }
        if ( iconName == "fa-binoculars" ) {
            return QString::fromUtf8( "\uf1e5" );
        }
        if ( iconName == "fa-newspaper-o" ) {
            return QString::fromUtf8( "\uf1ea" );
        }
        if ( iconName == "fa-sort-alpha-desc" ) {
            return QString::fromUtf8( "\uf15e" );
        }
        if ( iconName == "fa-reply" ) {
            return QString::fromUtf8( "\uf112" );
        }
        if ( iconName == "fa-twitter-square" ) {
            return QString::fromUtf8( "\uf081" );
        }
        if ( iconName == "fa-renren" ) {
            return QString::fromUtf8( "\uf18b" );
        }
        if ( iconName == "fa-flickr" ) {
            return QString::fromUtf8( "\uf16e" );
        }
        if ( iconName == "fa-warning" ) {
            return QString::fromUtf8( "\uf071" );
        }
        if ( iconName == "fa-shekel" ) {
            return QString::fromUtf8( "\uf20b" );
        }
        if ( iconName == "fa-caret-right" ) {
            return QString::fromUtf8( "\uf0da" );
        }
        if ( iconName == "fa-trello" ) {
            return QString::fromUtf8( "\uf181" );
        }
        if ( iconName == "fa-whatsapp" ) {
            return QString::fromUtf8( "\uf232" );
        }
        if ( iconName == "fa-retweet" ) {
            return QString::fromUtf8( "\uf079" );
        }
        if ( iconName == "fa-lastfm-square" ) {
            return QString::fromUtf8( "\uf203" );
        }
        if ( iconName == "fa-angle-double-down" ) {
            return QString::fromUtf8( "\uf103" );
        }
        if ( iconName == "fa-text-width" ) {
            return QString::fromUtf8( "\uf035" );
        }
        if ( iconName == "fa-mercury" ) {
            return QString::fromUtf8( "\uf223" );
        }
        if ( iconName == "fa-bed" ) {
            return QString::fromUtf8( "\uf236" );
        }
        if ( iconName == "fa-life-bouy" ) {
            return QString::fromUtf8( "\uf1cd" );
        }
        if ( iconName == "fa-star-half" ) {
            return QString::fromUtf8( "\uf089" );
        }
        if ( iconName == "fa-apple" ) {
            return QString::fromUtf8( "\uf179" );
        }
        if ( iconName == "fa-th" ) {
            return QString::fromUtf8( "\uf00a" );
        }
        if ( iconName == "fa-html5" ) {
            return QString::fromUtf8( "\uf13b" );
        }
        if ( iconName == "fa-jsfiddle" ) {
            return QString::fromUtf8( "\uf1cc" );
        }
        if ( iconName == "fa-sort-amount-desc" ) {
            return QString::fromUtf8( "\uf161" );
        }
        if ( iconName == "fa-genderless" ) {
            return QString::fromUtf8( "\uf1db" );
        }
        if ( iconName == "fa-spinner" ) {
            return QString::fromUtf8( "\uf110" );
        }
        if ( iconName == "fa-plus-square-o" ) {
            return QString::fromUtf8( "\uf196" );
        }
        if ( iconName == "fa-code-fork" ) {
            return QString::fromUtf8( "\uf126" );
        }
        if ( iconName == "fa-user" ) {
            return QString::fromUtf8( "\uf007" );
        }
        if ( iconName == "fa-automobile" ) {
            return QString::fromUtf8( "\uf1b9" );
        }
        if ( iconName == "fa-graduation-cap" ) {
            return QString::fromUtf8( "\uf19d" );
        }
        if ( iconName == "fa-suitcase" ) {
            return QString::fromUtf8( "\uf0f2" );
        }
        if ( iconName == "fa-angle-up" ) {
            return QString::fromUtf8( "\uf106" );
        }
        if ( iconName == "fa-train" ) {
            return QString::fromUtf8( "\uf238" );
        }
        if ( iconName == "fa-circle-o" ) {
            return QString::fromUtf8( "\uf10c" );
        }
        if ( iconName == "fa-star" ) {
            return QString::fromUtf8( "\uf005" );
        }
        if ( iconName == "fa-adn" ) {
            return QString::fromUtf8( "\uf170" );
        }
        if ( iconName == "fa-clipboard" ) {
            return QString::fromUtf8( "\uf0ea" );
        }
        if ( iconName == "fa-times" ) {
            return QString::fromUtf8( "\uf00d" );
        }
        if ( iconName == "fa-quote-left" ) {
            return QString::fromUtf8( "\uf10d" );
        }
        if ( iconName == "fa-angellist" ) {
            return QString::fromUtf8( "\uf209" );
        }
        if ( iconName == "fa-share-alt-square" ) {
            return QString::fromUtf8( "\uf1e1" );
        }
        if ( iconName == "fa-stumbleupon-circle" ) {
            return QString::fromUtf8( "\uf1a3" );
        }
        if ( iconName == "fa-building" ) {
            return QString::fromUtf8( "\uf1ad" );
        }
        if ( iconName == "fa-rouble" ) {
            return QString::fromUtf8( "\uf158" );
        }
        if ( iconName == "fa-comment-o" ) {
            return QString::fromUtf8( "\uf0e5" );
        }
        if ( iconName == "fa-support" ) {
            return QString::fromUtf8( "\uf1cd" );
        }
        if ( iconName == "fa-dot-circle-o" ) {
            return QString::fromUtf8( "\uf192" );
        }
        if ( iconName == "fa-list" ) {
            return QString::fromUtf8( "\uf03a" );
        }
        if ( iconName == "fa-rupee" ) {
            return QString::fromUtf8( "\uf156" );
        }
        if ( iconName == "fa-share-square-o" ) {
            return QString::fromUtf8( "\uf045" );
        }
        if ( iconName == "fa-vk" ) {
            return QString::fromUtf8( "\uf189" );
        }
        if ( iconName == "fa-mortar-board" ) {
            return QString::fromUtf8( "\uf19d" );
        }
        if ( iconName == "fa-minus-circle" ) {
            return QString::fromUtf8( "\uf056" );
        }
        if ( iconName == "fa-filter" ) {
            return QString::fromUtf8( "\uf0b0" );
        }
        if ( iconName == "fa-wordpress" ) {
            return QString::fromUtf8( "\uf19a" );
        }
        if ( iconName == "fa-tint" ) {
            return QString::fromUtf8( "\uf043" );
        }
        if ( iconName == "fa-long-arrow-right" ) {
            return QString::fromUtf8( "\uf178" );
        }
        if ( iconName == "fa-file-movie-o" ) {
            return QString::fromUtf8( "\uf1c8" );
        }
        if ( iconName == "fa-bullseye" ) {
            return QString::fromUtf8( "\uf140" );
        }
        if ( iconName == "fa-file-sound-o" ) {
            return QString::fromUtf8( "\uf1c7" );
        }
        if ( iconName == "fa-credit-card" ) {
            return QString::fromUtf8( "\uf09d" );
        }
        if ( iconName == "fa-vimeo-square" ) {
            return QString::fromUtf8( "\uf194" );
        }
        if ( iconName == "fa-money" ) {
            return QString::fromUtf8( "\uf0d6" );
        }
        if ( iconName == "fa-sort-numeric-asc" ) {
            return QString::fromUtf8( "\uf162" );
        }
        if ( iconName == "fa-arrow-circle-down" ) {
            return QString::fromUtf8( "\uf0ab" );
        }
        if ( iconName == "fa-tumblr" ) {
            return QString::fromUtf8( "\uf173" );
        }
        if ( iconName == "fa-download" ) {
            return QString::fromUtf8( "\uf019" );
        }
        if ( iconName == "fa-minus-square-o" ) {
            return QString::fromUtf8( "\uf147" );
        }
        if ( iconName == "fa-ship" ) {
            return QString::fromUtf8( "\uf21a" );
        }
        if ( iconName == "fa-ambulance" ) {
            return QString::fromUtf8( "\uf0f9" );
        }
        if ( iconName == "fa-unlock" ) {
            return QString::fromUtf8( "\uf09c" );
        }
        if ( iconName == "fa-reddit" ) {
            return QString::fromUtf8( "\uf1a1" );
        }
        if ( iconName == "fa-video-camera" ) {
            return QString::fromUtf8( "\uf03d" );
        }
        if ( iconName == "fa-level-down" ) {
            return QString::fromUtf8( "\uf149" );
        }
        if ( iconName == "fa-paper-plane" ) {
            return QString::fromUtf8( "\uf1d8" );
        }
        if ( iconName == "fa-bar-chart-o" ) {
            return QString::fromUtf8( "\uf080" );
        }
        if ( iconName == "fa-thumbs-o-down" ) {
            return QString::fromUtf8( "\uf088" );
        }
        if ( iconName == "fa-space-shuttle" ) {
            return QString::fromUtf8( "\uf197" );
        }
        if ( iconName == "fa-file-pdf-o" ) {
            return QString::fromUtf8( "\uf1c1" );
        }
        if ( iconName == "fa-exclamation-triangle" ) {
            return QString::fromUtf8( "\uf071" );
        }
        if ( iconName == "fa-facebook-f" ) {
            return QString::fromUtf8( "\uf09a" );
        }
        if ( iconName == "fa-youtube-square" ) {
            return QString::fromUtf8( "\uf166" );
        }
        if ( iconName == "fa-hospital-o" ) {
            return QString::fromUtf8( "\uf0f8" );
        }
        if ( iconName == "fa-google-plus-square" ) {
            return QString::fromUtf8( "\uf0d4" );
        }
        if ( iconName == "fa-file-word-o" ) {
            return QString::fromUtf8( "\uf1c2" );
        }
        if ( iconName == "fa-stack-overflow" ) {
            return QString::fromUtf8( "\uf16c" );
        }
        if ( iconName == "fa-magic" ) {
            return QString::fromUtf8( "\uf0d0" );
        }
        if ( iconName == "fa-usd" ) {
            return QString::fromUtf8( "\uf155" );
        }
        if ( iconName == "fa-calendar" ) {
            return QString::fromUtf8( "\uf073" );
        }
        if ( iconName == "fa-eject" ) {
            return QString::fromUtf8( "\uf052" );
        }
        if ( iconName == "fa-code" ) {
            return QString::fromUtf8( "\uf121" );
        }
        if ( iconName == "fa-font" ) {
            return QString::fromUtf8( "\uf031" );
        }
        if ( iconName == "fa-rocket" ) {
            return QString::fromUtf8( "\uf135" );
        }
        if ( iconName == "fa-pinterest" ) {
            return QString::fromUtf8( "\uf0d2" );
        }
        if ( iconName == "fa-viacoin" ) {
            return QString::fromUtf8( "\uf237" );
        }
        if ( iconName == "fa-external-link-square" ) {
            return QString::fromUtf8( "\uf14c" );
        }
        if ( iconName == "fa-envelope-square" ) {
            return QString::fromUtf8( "\uf199" );
        }
        if ( iconName == "fa-external-link" ) {
            return QString::fromUtf8( "\uf08e" );
        }
        if ( iconName == "fa-deviantart" ) {
            return QString::fromUtf8( "\uf1bd" );
        }
        if ( iconName == "fa-square" ) {
            return QString::fromUtf8( "\uf0c8" );
        }
        if ( iconName == "fa-pencil-square" ) {
            return QString::fromUtf8( "\uf14b" );
        }
        if ( iconName == "fa-rmb" ) {
            return QString::fromUtf8( "\uf157" );
        }
        if ( iconName == "fa-italic" ) {
            return QString::fromUtf8( "\uf033" );
        }
        if ( iconName == "fa-smile-o" ) {
            return QString::fromUtf8( "\uf118" );
        }
        if ( iconName == "fa-microphone" ) {
            return QString::fromUtf8( "\uf130" );
        }
        if ( iconName == "fa-coffee" ) {
            return QString::fromUtf8( "\uf0f4" );
        }
        if ( iconName == "fa-hand-o-up" ) {
            return QString::fromUtf8( "\uf0a6" );
        }
        if ( iconName == "fa-expand" ) {
            return QString::fromUtf8( "\uf065" );
        }
        if ( iconName == "fa-subway" ) {
            return QString::fromUtf8( "\uf239" );
        }
        if ( iconName == "fa-file-powerpoint-o" ) {
            return QString::fromUtf8( "\uf1c4" );
        }
        if ( iconName == "fa-angle-double-up" ) {
            return QString::fromUtf8( "\uf102" );
        }
        if ( iconName == "fa-paypal" ) {
            return QString::fromUtf8( "\uf1ed" );
        }
        if ( iconName == "fa-fast-backward" ) {
            return QString::fromUtf8( "\uf049" );
        }
        if ( iconName == "fa-ioxhost" ) {
            return QString::fromUtf8( "\uf208" );
        }
        if ( iconName == "fa-bell" ) {
            return QString::fromUtf8( "\uf0f3" );
        }
        if ( iconName == "fa-lock" ) {
            return QString::fromUtf8( "\uf023" );
        }
        if ( iconName == "fa-arrow-circle-up" ) {
            return QString::fromUtf8( "\uf0aa" );
        }
        if ( iconName == "fa-trash-o" ) {
            return QString::fromUtf8( "\uf014" );
        }
        if ( iconName == "fa-cutlery" ) {
            return QString::fromUtf8( "\uf0f5" );
        }
        if ( iconName == "fa-shield" ) {
            return QString::fromUtf8( "\uf132" );
        }
        if ( iconName == "fa-line-chart" ) {
            return QString::fromUtf8( "\uf201" );
        }
        if ( iconName == "fa-share-square" ) {
            return QString::fromUtf8( "\uf14d" );
        }
        if ( iconName == "fa-tree" ) {
            return QString::fromUtf8( "\uf1bb" );
        }
        if ( iconName == "fa-glass" ) {
            return QString::fromUtf8( "\uf000" );
        }
        if ( iconName == "fa-try" ) {
            return QString::fromUtf8( "\uf195" );
        }
        if ( iconName == "fa-minus" ) {
            return QString::fromUtf8( "\uf068" );
        }
        if ( iconName == "fa-search-minus" ) {
            return QString::fromUtf8( "\uf010" );
        }
        if ( iconName == "fa-sort-numeric-desc" ) {
            return QString::fromUtf8( "\uf163" );
        }
        if ( iconName == "fa-barcode" ) {
            return QString::fromUtf8( "\uf02a" );
        }
        if ( iconName == "fa-chevron-circle-down" ) {
            return QString::fromUtf8( "\uf13a" );
        }
        if ( iconName == "fa-unlock-alt" ) {
            return QString::fromUtf8( "\uf13e" );
        }
        if ( iconName == "fa-square-o" ) {
            return QString::fromUtf8( "\uf096" );
        }
        if ( iconName == "fa-sort-desc" ) {
            return QString::fromUtf8( "\uf0dd" );
        }
        if ( iconName == "fa-gift" ) {
            return QString::fromUtf8( "\uf06b" );
        }
        if ( iconName == "fa-skyatlas" ) {
            return QString::fromUtf8( "\uf216" );
        }
        if ( iconName == "fa-star-half-full" ) {
            return QString::fromUtf8( "\uf123" );
        }
        if ( iconName == "fa-language" ) {
            return QString::fromUtf8( "\uf1ab" );
        }
        if ( iconName == "fa-signal" ) {
            return QString::fromUtf8( "\uf012" );
        }
        if ( iconName == "fa-volume-off" ) {
            return QString::fromUtf8( "\uf026" );
        }
        if ( iconName == "fa-long-arrow-down" ) {
            return QString::fromUtf8( "\uf175" );
        }
        if ( iconName == "fa-sign-out" ) {
            return QString::fromUtf8( "\uf08b" );
        }
        if ( iconName == "fa-keyboard-o" ) {
            return QString::fromUtf8( "\uf11c" );
        }
        if ( iconName == "fa-angle-down" ) {
            return QString::fromUtf8( "\uf107" );
        }
        if ( iconName == "fa-close" ) {
            return QString::fromUtf8( "\uf00d" );
        }
        if ( iconName == "fa-image" ) {
            return QString::fromUtf8( "\uf03e" );
        }
        if ( iconName == "fa-adjust" ) {
            return QString::fromUtf8( "\uf042" );
        }
        if ( iconName == "fa-arrow-circle-o-left" ) {
            return QString::fromUtf8( "\uf190" );
        }
        if ( iconName == "fa-delicious" ) {
            return QString::fromUtf8( "\uf1a5" );
        }
        if ( iconName == "fa-random" ) {
            return QString::fromUtf8( "\uf074" );
        }
        if ( iconName == "fa-music" ) {
            return QString::fromUtf8( "\uf001" );
        }
        if ( iconName == "fa-transgender" ) {
            return QString::fromUtf8( "\uf224" );
        }
        if ( iconName == "fa-cny" ) {
            return QString::fromUtf8( "\uf157" );
        }
        if ( iconName == "fa-ruble" ) {
            return QString::fromUtf8( "\uf158" );
        }
        if ( iconName == "fa-anchor" ) {
            return QString::fromUtf8( "\uf13d" );
        }
        if ( iconName == "fa-cog" ) {
            return QString::fromUtf8( "\uf013" );
        }
        if ( iconName == "fa-play-circle-o" ) {
            return QString::fromUtf8( "\uf01d" );
        }
        if ( iconName == "fa-search-plus" ) {
            return QString::fromUtf8( "\uf00e" );
        }
        if ( iconName == "fa-file-image-o" ) {
            return QString::fromUtf8( "\uf1c5" );
        }
        if ( iconName == "fa-fire-extinguisher" ) {
            return QString::fromUtf8( "\uf134" );
        }
        if ( iconName == "fa-pagelines" ) {
            return QString::fromUtf8( "\uf18c" );
        }
        if ( iconName == "fa-cogs" ) {
            return QString::fromUtf8( "\uf085" );
        }
        if ( iconName == "fa-times-circle" ) {
            return QString::fromUtf8( "\uf057" );
        }
        if ( iconName == "fa-behance" ) {
            return QString::fromUtf8( "\uf1b4" );
        }
        if ( iconName == "fa-maxcdn" ) {
            return QString::fromUtf8( "\uf136" );
        }
        if ( iconName == "fa-caret-down" ) {
            return QString::fromUtf8( "\uf0d7" );
        }
        if ( iconName == "fa-android" ) {
            return QString::fromUtf8( "\uf17b" );
        }
        if ( iconName == "fa-gratipay" ) {
            return QString::fromUtf8( "\uf184" );
        }
        if ( iconName == "fa-tty" ) {
            return QString::fromUtf8( "\uf1e4" );
        }
        if ( iconName == "fa-hand-o-left" ) {
            return QString::fromUtf8( "\uf0a5" );
        }
        if ( iconName == "fa-thumbs-down" ) {
            return QString::fromUtf8( "\uf165" );
        }
        if ( iconName == "fa-underline" ) {
            return QString::fromUtf8( "\uf0cd" );
        }
        if ( iconName == "fa-facebook-official" ) {
            return QString::fromUtf8( "\uf230" );
        }
        if ( iconName == "fa-superscript" ) {
            return QString::fromUtf8( "\uf12b" );
        }
        if ( iconName == "fa-phone" ) {
            return QString::fromUtf8( "\uf095" );
        }
        if ( iconName == "fa-css3" ) {
            return QString::fromUtf8( "\uf13c" );
        }
        if ( iconName == "fa-cloud" ) {
            return QString::fromUtf8( "\uf0c2" );
        }
        if ( iconName == "fa-diamond" ) {
            return QString::fromUtf8( "\uf219" );
        }
        if ( iconName == "fa-crop" ) {
            return QString::fromUtf8( "\uf125" );
        }
        if ( iconName == "fa-joomla" ) {
            return QString::fromUtf8( "\uf1aa" );
        }
        if ( iconName == "fa-sort-alpha-asc" ) {
            return QString::fromUtf8( "\uf15d" );
        }
        if ( iconName == "fa-star-o" ) {
            return QString::fromUtf8( "\uf006" );
        }
        if ( iconName == "fa-male" ) {
            return QString::fromUtf8( "\uf183" );
        }
        if ( iconName == "fa-spotify" ) {
            return QString::fromUtf8( "\uf1bc" );
        }
        if ( iconName == "fa-sun-o" ) {
            return QString::fromUtf8( "\uf185" );
        }
        if ( iconName == "fa-step-backward" ) {
            return QString::fromUtf8( "\uf048" );
        }
        if ( iconName == "fa-yahoo" ) {
            return QString::fromUtf8( "\uf19e" );
        }
        if ( iconName == "fa-mars-double" ) {
            return QString::fromUtf8( "\uf227" );
        }
        if ( iconName == "fa-chain" ) {
            return QString::fromUtf8( "\uf0c1" );
        }
        if ( iconName == "fa-ticket" ) {
            return QString::fromUtf8( "\uf145" );
        }
        if ( iconName == "fa-eur" ) {
            return QString::fromUtf8( "\uf153" );
        }
        if ( iconName == "fa-user-secret" ) {
            return QString::fromUtf8( "\uf21b" );
        }
        if ( iconName == "fa-copyright" ) {
            return QString::fromUtf8( "\uf1f9" );
        }
        if ( iconName == "fa-inbox" ) {
            return QString::fromUtf8( "\uf01c" );
        }
        if ( iconName == "fa-tablet" ) {
            return QString::fromUtf8( "\uf10a" );
        }
        if ( iconName == "fa-outdent" ) {
            return QString::fromUtf8( "\uf03b" );
        }
        if ( iconName == "fa-magnet" ) {
            return QString::fromUtf8( "\uf076" );
        }
        if ( iconName == "fa-bicycle" ) {
            return QString::fromUtf8( "\uf206" );
        }
        if ( iconName == "fa-shirtsinbulk" ) {
            return QString::fromUtf8( "\uf214" );
        }
        if ( iconName == "fa-microphone-slash" ) {
            return QString::fromUtf8( "\uf131" );
        }
        if ( iconName == "fa-gear" ) {
            return QString::fromUtf8( "\uf013" );
        }
        if ( iconName == "fa-cc-stripe" ) {
            return QString::fromUtf8( "\uf1f5" );
        }
        if ( iconName == "fa-recycle" ) {
            return QString::fromUtf8( "\uf1b8" );
        }
        if ( iconName == "fa-forumbee" ) {
            return QString::fromUtf8( "\uf211" );
        }
        if ( iconName == "fa-moon-o" ) {
            return QString::fromUtf8( "\uf186" );
        }
        if ( iconName == "fa-undo" ) {
            return QString::fromUtf8( "\uf0e2" );
        }
        if ( iconName == "fa-trash" ) {
            return QString::fromUtf8( "\uf1f8" );
        }
        if ( iconName == "fa-angle-double-left" ) {
            return QString::fromUtf8( "\uf100" );
        }
        if ( iconName == "fa-volume-down" ) {
            return QString::fromUtf8( "\uf027" );
        }
        if ( iconName == "fa-futbol-o" ) {
            return QString::fromUtf8( "\uf1e3" );
        }
        if ( iconName == "fa-share-alt" ) {
            return QString::fromUtf8( "\uf1e0" );
        }
        if ( iconName == "fa-headphones" ) {
            return QString::fromUtf8( "\uf025" );
        }
        if ( iconName == "fa-cc-mastercard" ) {
            return QString::fromUtf8( "\uf1f1" );
        }
        if ( iconName == "fa-street-view" ) {
            return QString::fromUtf8( "\uf21d" );
        }
        if ( iconName == "fa-lastfm" ) {
            return QString::fromUtf8( "\uf202" );
        }
        if ( iconName == "fa-arrow-down" ) {
            return QString::fromUtf8( "\uf063" );
        }
        if ( iconName == "fa-camera" ) {
            return QString::fromUtf8( "\uf030" );
        }
        if ( iconName == "fa-meanpath" ) {
            return QString::fromUtf8( "\uf20c" );
        }
        if ( iconName == "fa-buysellads" ) {
            return QString::fromUtf8( "\uf20d" );
        }
        if ( iconName == "fa-rss" ) {
            return QString::fromUtf8( "\uf09e" );
        }
        if ( iconName == "fa-dollar" ) {
            return QString::fromUtf8( "\uf155" );
        }
        if ( iconName == "fa-times-circle-o" ) {
            return QString::fromUtf8( "\uf05c" );
        }
        if ( iconName == "fa-bullhorn" ) {
            return QString::fromUtf8( "\uf0a1" );
        }
        if ( iconName == "fa-thumbs-up" ) {
            return QString::fromUtf8( "\uf164" );
        }
        if ( iconName == "fa-wechat" ) {
            return QString::fromUtf8( "\uf1d7" );
        }
        if ( iconName == "fa-sellsy" ) {
            return QString::fromUtf8( "\uf213" );
        }
        if ( iconName == "fa-arrow-circle-o-up" ) {
            return QString::fromUtf8( "\uf01b" );
        }
        if ( iconName == "fa-cc-paypal" ) {
            return QString::fromUtf8( "\uf1f4" );
        }
        if ( iconName == "fa-comments" ) {
            return QString::fromUtf8( "\uf086" );
        }
        if ( iconName == "fa-toggle-right" ) {
            return QString::fromUtf8( "\uf152" );
        }
        if ( iconName == "fa-wheelchair" ) {
            return QString::fromUtf8( "\uf193" );
        }
        if ( iconName == "fa-chevron-circle-up" ) {
            return QString::fromUtf8( "\uf139" );
        }
        if ( iconName == "fa-venus" ) {
            return QString::fromUtf8( "\uf221" );
        }
        if ( iconName == "fa-drupal" ) {
            return QString::fromUtf8( "\uf1a9" );
        }
        if ( iconName == "fa-backward" ) {
            return QString::fromUtf8( "\uf04a" );
        }
        if ( iconName == "fa-refresh" ) {
            return QString::fromUtf8( "\uf021" );
        }
        if ( iconName == "fa-rub" ) {
            return QString::fromUtf8( "\uf158" );
        }
        if ( iconName == "fa-stethoscope" ) {
            return QString::fromUtf8( "\uf0f1" );
        }
        if ( iconName == "fa-eye" ) {
            return QString::fromUtf8( "\uf06e" );
        }
        if ( iconName == "fa-github-square" ) {
            return QString::fromUtf8( "\uf092" );
        }
        if ( iconName == "fa-desktop" ) {
            return QString::fromUtf8( "\uf108" );
        }
        if ( iconName == "fa-terminal" ) {
            return QString::fromUtf8( "\uf120" );
        }
        if ( iconName == "fa-git-square" ) {
            return QString::fromUtf8( "\uf1d2" );
        }
        if ( iconName == "fa-tachometer" ) {
            return QString::fromUtf8( "\uf0e4" );
        }
        if ( iconName == "fa-stack-exchange" ) {
            return QString::fromUtf8( "\uf18d" );
        }
        if ( iconName == "fa-file-o" ) {
            return QString::fromUtf8( "\uf016" );
        }
        if ( iconName == "fa-check-square" ) {
            return QString::fromUtf8( "\uf14a" );
        }
        if ( iconName == "fa-key" ) {
            return QString::fromUtf8( "\uf084" );
        }
        if ( iconName == "fa-chevron-circle-right" ) {
            return QString::fromUtf8( "\uf138" );
        }
        if ( iconName == "fa-slideshare" ) {
            return QString::fromUtf8( "\uf1e7" );
        }
        if ( iconName == "fa-tags" ) {
            return QString::fromUtf8( "\uf02c" );
        }
        if ( iconName == "fa-life-buoy" ) {
            return QString::fromUtf8( "\uf1cd" );
        }
        if ( iconName == "fa-arrows-alt" ) {
            return QString::fromUtf8( "\uf0b2" );
        }
        if ( iconName == "fa-minus-square" ) {
            return QString::fromUtf8( "\uf146" );
        }
        if ( iconName == "fa-file-archive-o" ) {
            return QString::fromUtf8( "\uf1c6" );
        }
        if ( iconName == "fa-dropbox" ) {
            return QString::fromUtf8( "\uf16b" );
        }
        if ( iconName == "fa-flash" ) {
            return QString::fromUtf8( "\uf0e7" );
        }
        if ( iconName == "fa-flask" ) {
            return QString::fromUtf8( "\uf0c3" );
        }
        if ( iconName == "fa-reddit-square" ) {
            return QString::fromUtf8( "\uf1a2" );
        }
        if ( iconName == "fa-mail-reply" ) {
            return QString::fromUtf8( "\uf112" );
        }
        if ( iconName == "fa-arrow-circle-left" ) {
            return QString::fromUtf8( "\uf0a8" );
        }
        if ( iconName == "fa-cut" ) {
            return QString::fromUtf8( "\uf0c4" );
        }
        if ( iconName == "fa-tencent-weibo" ) {
            return QString::fromUtf8( "\uf1d5" );
        }
        if ( iconName == "fa-file-audio-o" ) {
            return QString::fromUtf8( "\uf1c7" );
        }
        if ( iconName == "fa-cc-visa" ) {
            return QString::fromUtf8( "\uf1f0" );
        }
        if ( iconName == "fa-dashcube" ) {
            return QString::fromUtf8( "\uf210" );
        }
        if ( iconName == "fa-file-zip-o" ) {
            return QString::fromUtf8( "\uf1c6" );
        }
        if ( iconName == "fa-arrows" ) {
            return QString::fromUtf8( "\uf047" );
        }
        if ( iconName == "fa-check-circle-o" ) {
            return QString::fromUtf8( "\uf05d" );
        }
        if ( iconName == "fa-send" ) {
            return QString::fromUtf8( "\uf1d8" );
        }
        if ( iconName == "fa-mars-stroke" ) {
            return QString::fromUtf8( "\uf229" );
        }
        if ( iconName == "fa-youtube-play" ) {
            return QString::fromUtf8( "\uf16a" );
        }
        if ( iconName == "fa-linux" ) {
            return QString::fromUtf8( "\uf17c" );
        }
        if ( iconName == "fa-female" ) {
            return QString::fromUtf8( "\uf182" );
        }
        if ( iconName == "fa-yen" ) {
            return QString::fromUtf8( "\uf157" );
        }
        if ( iconName == "fa-at" ) {
            return QString::fromUtf8( "\uf1fa" );
        }
        if ( iconName == "fa-btc" ) {
            return QString::fromUtf8( "\uf15a" );
        }
        if ( iconName == "fa-pencil" ) {
            return QString::fromUtf8( "\uf040" );
        }
        if ( iconName == "fa-photo" ) {
            return QString::fromUtf8( "\uf03e" );
        }
        if ( iconName == "fa-medium" ) {
            return QString::fromUtf8( "\uf23a" );
        }
        if ( iconName == "fa-empire" ) {
            return QString::fromUtf8( "\uf1d1" );
        }
        if ( iconName == "fa-xing-square" ) {
            return QString::fromUtf8( "\uf169" );
        }
        if ( iconName == "fa-stop" ) {
            return QString::fromUtf8( "\uf04d" );
        }
        if ( iconName == "fa-toggle-down" ) {
            return QString::fromUtf8( "\uf150" );
        }
        if ( iconName == "fa-hdd-o" ) {
            return QString::fromUtf8( "\uf0a0" );
        }
        if ( iconName == "fa-flag-checkered" ) {
            return QString::fromUtf8( "\uf11e" );
        }
        if ( iconName == "fa-sort-asc" ) {
            return QString::fromUtf8( "\uf0de" );
        }
        if ( iconName == "fa-bitbucket-square" ) {
            return QString::fromUtf8( "\uf172" );
        }
        if ( iconName == "fa-eye-slash" ) {
            return QString::fromUtf8( "\uf070" );
        }
        if ( iconName == "fa-file" ) {
            return QString::fromUtf8( "\uf15b" );
        }
        if ( iconName == "fa-database" ) {
            return QString::fromUtf8( "\uf1c0" );
        }
        if ( iconName == "fa-road" ) {
            return QString::fromUtf8( "\uf018" );
        }
        if ( iconName == "fa-mars" ) {
            return QString::fromUtf8( "\uf222" );
        }
        if ( iconName == "fa-camera-retro" ) {
            return QString::fromUtf8( "\uf083" );
        }
        if ( iconName == "fa-film" ) {
            return QString::fromUtf8( "\uf008" );
        }
        if ( iconName == "fa-hacker-news" ) {
            return QString::fromUtf8( "\uf1d4" );
        }
        if ( iconName == "fa-rotate-right" ) {
            return QString::fromUtf8( "\uf01e" );
        }
        if ( iconName == "fa-th-list" ) {
            return QString::fromUtf8( "\uf00b" );
        }
        if ( iconName == "fa-bold" ) {
            return QString::fromUtf8( "\uf032" );
        }
        if ( iconName == "fa-navicon" ) {
            return QString::fromUtf8( "\uf0c9" );
        }
        if ( iconName == "fa-gavel" ) {
            return QString::fromUtf8( "\uf0e3" );
        }
        if ( iconName == "fa-bug" ) {
            return QString::fromUtf8( "\uf188" );
        }
        if ( iconName == "fa-comments-o" ) {
            return QString::fromUtf8( "\uf0e6" );
        }
        if ( iconName == "fa-facebook" ) {
            return QString::fromUtf8( "\uf09a" );
        }
        if ( iconName == "fa-bell-slash-o" ) {
            return QString::fromUtf8( "\uf1f7" );
        }
        if ( iconName == "fa-mail-reply-all" ) {
            return QString::fromUtf8( "\uf122" );
        }
        if ( iconName == "fa-plug" ) {
            return QString::fromUtf8( "\uf1e6" );
        }
        if ( iconName == "fa-cc" ) {
            return QString::fromUtf8( "\uf20a" );
        }
        if ( iconName == "fa-caret-square-o-down" ) {
            return QString::fromUtf8( "\uf150" );
        }
        if ( iconName == "fa-birthday-cake" ) {
            return QString::fromUtf8( "\uf1fd" );
        }
        if ( iconName == "fa-truck" ) {
            return QString::fromUtf8( "\uf0d1" );
        }
        if ( iconName == "fa-angle-right" ) {
            return QString::fromUtf8( "\uf105" );
        }
        if ( iconName == "fa-bus" ) {
            return QString::fromUtf8( "\uf207" );
        }
        if ( iconName == "fa-bolt" ) {
            return QString::fromUtf8( "\uf0e7" );
        }
        if ( iconName == "fa-bank" ) {
            return QString::fromUtf8( "\uf19c" );
        }
        if ( iconName == "fa-building-o" ) {
            return QString::fromUtf8( "\uf0f7" );
        }
        if ( iconName == "fa-exchange" ) {
            return QString::fromUtf8( "\uf0ec" );
        }
        if ( iconName == "fa-plus" ) {
            return QString::fromUtf8( "\uf067" );
        }
        if ( iconName == "fa-life-ring" ) {
            return QString::fromUtf8( "\uf1cd" );
        }
        if ( iconName == "fa-frown-o" ) {
            return QString::fromUtf8( "\uf119" );
        }
        if ( iconName == "fa-unsorted" ) {
            return QString::fromUtf8( "\uf0dc" );
        }
        if ( iconName == "fa-bomb" ) {
            return QString::fromUtf8( "\uf1e2" );
        }
        if ( iconName == "fa-arrow-circle-right" ) {
            return QString::fromUtf8( "\uf0a9" );
        }
        if ( iconName == "fa-pied-piper" ) {
            return QString::fromUtf8( "\uf1a7" );
        }
        if ( iconName == "fa-map-marker" ) {
            return QString::fromUtf8( "\uf041" );
        }
        if ( iconName == "fa-wifi" ) {
            return QString::fromUtf8( "\uf1eb" );
        }
        if ( iconName == "fa-paragraph" ) {
            return QString::fromUtf8( "\uf1dd" );
        }
        if ( iconName == "fa-twitch" ) {
            return QString::fromUtf8( "\uf1e8" );
        }
        if ( iconName == "fa-pinterest-square" ) {
            return QString::fromUtf8( "\uf0d3" );
        }
        if ( iconName == "fa-copy" ) {
            return QString::fromUtf8( "\uf0c5" );
        }
        if ( iconName == "fa-bitcoin" ) {
            return QString::fromUtf8( "\uf15a" );
        }
        if ( iconName == "fa-venus-double" ) {
            return QString::fromUtf8( "\uf226" );
        }
        if ( iconName == "fa-mobile-phone" ) {
            return QString::fromUtf8( "\uf10b" );
        }
        if ( iconName == "fa-thumbs-o-up" ) {
            return QString::fromUtf8( "\uf087" );
        }
        if ( iconName == "fa-dedent" ) {
            return QString::fromUtf8( "\uf03b" );
        }
        if ( iconName == "fa-list-alt" ) {
            return QString::fromUtf8( "\uf022" );
        }
        if ( iconName == "fa-cart-arrow-down" ) {
            return QString::fromUtf8( "\uf218" );
        }
        if ( iconName == "fa-chevron-circle-left" ) {
            return QString::fromUtf8( "\uf137" );
        }
        if ( iconName == "fa-digg" ) {
            return QString::fromUtf8( "\uf1a6" );
        }
        if ( iconName == "fa-subscript" ) {
            return QString::fromUtf8( "\uf12c" );
        }
        if ( iconName == "fa-thumb-tack" ) {
            return QString::fromUtf8( "\uf08d" );
        }
        if ( iconName == "fa-quote-right" ) {
            return QString::fromUtf8( "\uf10e" );
        }
        if ( iconName == "fa-align-center" ) {
            return QString::fromUtf8( "\uf037" );
        }
        if ( iconName == "fa-repeat" ) {
            return QString::fromUtf8( "\uf01e" );
        }
        if ( iconName == "fa-bar-chart" ) {
            return QString::fromUtf8( "\uf080" );
        }
        if ( iconName == "fa-circle-thin" ) {
            return QString::fromUtf8( "\uf1db" );
        }
        if ( iconName == "fa-upload" ) {
            return QString::fromUtf8( "\uf093" );
        }
        if ( iconName == "fa-long-arrow-left" ) {
            return QString::fromUtf8( "\uf177" );
        }
        if ( iconName == "fa-angle-left" ) {
            return QString::fromUtf8( "\uf104" );
        }
        if ( iconName == "fa-columns" ) {
            return QString::fromUtf8( "\uf0db" );
        }
        if ( iconName == "fa-align-justify" ) {
            return QString::fromUtf8( "\uf039" );
        }
        if ( iconName == "fa-text-height" ) {
            return QString::fromUtf8( "\uf034" );
        }
        if ( iconName == "fa-turkish-lira" ) {
            return QString::fromUtf8( "\uf195" );
        }
        if ( iconName == "fa-file-excel-o" ) {
            return QString::fromUtf8( "\uf1c3" );
        }
        if ( iconName == "fa-windows" ) {
            return QString::fromUtf8( "\uf17a" );
        }
        if ( iconName == "fa-chevron-right" ) {
            return QString::fromUtf8( "\uf054" );
        }
        if ( iconName == "fa-steam" ) {
            return QString::fromUtf8( "\uf1b6" );
        }
        if ( iconName == "fa-trophy" ) {
            return QString::fromUtf8( "\uf091" );
        }
        if ( iconName == "fa-life-saver" ) {
            return QString::fromUtf8( "\uf1cd" );
        }
        if ( iconName == "fa-wrench" ) {
            return QString::fromUtf8( "\uf0ad" );
        }
        if ( iconName == "fa-book" ) {
            return QString::fromUtf8( "\uf02d" );
        }
        if ( iconName == "fa-sheqel" ) {
            return QString::fromUtf8( "\uf20b" );
        }
        if ( iconName == "fa-umbrella" ) {
            return QString::fromUtf8( "\uf0e9" );
        }
        if ( iconName == "fa-send-o" ) {
            return QString::fromUtf8( "\uf1d9" );
        }
        if ( iconName == "fa-google-wallet" ) {
            return QString::fromUtf8( "\uf1ee" );
        }
        if ( iconName == "fa-gbp" ) {
            return QString::fromUtf8( "\uf154" );
        }
        if ( iconName == "fa-caret-left" ) {
            return QString::fromUtf8( "\uf0d9" );
        }
        if ( iconName == "fa-cloud-download" ) {
            return QString::fromUtf8( "\uf0ed" );
        }
        if ( iconName == "fa-plus-circle" ) {
            return QString::fromUtf8( "\uf055" );
        }
        if ( iconName == "fa-openid" ) {
            return QString::fromUtf8( "\uf19b" );
        }
        if ( iconName == "fa-pied-piper-alt" ) {
            return QString::fromUtf8( "\uf1a8" );
        }
        if ( iconName == "fa-align-left" ) {
            return QString::fromUtf8( "\uf036" );
        }
        if ( iconName == "fa-skype" ) {
            return QString::fromUtf8( "\uf17e" );
        }
        if ( iconName == "fa-puzzle-piece" ) {
            return QString::fromUtf8( "\uf12e" );
        }
        if ( iconName == "fa-bell-o" ) {
            return QString::fromUtf8( "\uf0a2" );
        }
        if ( iconName == "fa-share" ) {
            return QString::fromUtf8( "\uf064" );
        }
        if ( iconName == "fa-volume-up" ) {
            return QString::fromUtf8( "\uf028" );
        }
        if ( iconName == "fa-heartbeat" ) {
            return QString::fromUtf8( "\uf21e" );
        }
        if ( iconName == "fa-rss-square" ) {
            return QString::fromUtf8( "\uf143" );
        }
        if ( iconName == "fa-dribbble" ) {
            return QString::fromUtf8( "\uf17d" );
        }
        if ( iconName == "fa-github-alt" ) {
            return QString::fromUtf8( "\uf113" );
        }
        if ( iconName == "fa-folder-o" ) {
            return QString::fromUtf8( "\uf114" );
        }
        if ( iconName == "fa-search" ) {
            return QString::fromUtf8( "\uf002" );
        }
        if ( iconName == "fa-file-picture-o" ) {
            return QString::fromUtf8( "\uf1c5" );
        }
        if ( iconName == "fa-ge" ) {
            return QString::fromUtf8( "\uf1d1" );
        }
        if ( iconName == "fa-strikethrough" ) {
            return QString::fromUtf8( "\uf0cc" );
        }
        if ( iconName == "fa-twitter" ) {
            return QString::fromUtf8( "\uf099" );
        }
        if ( iconName == "fa-server" ) {
            return QString::fromUtf8( "\uf233" );
        }
        if ( iconName == "fa-bars" ) {
            return QString::fromUtf8( "\uf0c9" );
        }
        if ( iconName == "fa-fax" ) {
            return QString::fromUtf8( "\uf1ac" );
        }
        if ( iconName == "fa-fast-forward" ) {
            return QString::fromUtf8( "\uf050" );
        }
        if ( iconName == "fa-paper-plane-o" ) {
            return QString::fromUtf8( "\uf1d9" );
        }
        if ( iconName == "fa-chevron-down" ) {
            return QString::fromUtf8( "\uf078" );
        }
        if ( iconName == "fa-play-circle" ) {
            return QString::fromUtf8( "\uf144" );
        }
        if ( iconName == "fa-bell-slash" ) {
            return QString::fromUtf8( "\uf1f6" );
        }
        if ( iconName == "fa-print" ) {
            return QString::fromUtf8( "\uf02f" );
        }
        if ( iconName == "fa-bookmark-o" ) {
            return QString::fromUtf8( "\uf097" );
        }
        if ( iconName == "fa-star-half-empty" ) {
            return QString::fromUtf8( "\uf123" );
        }
        if ( iconName == "fa-heart" ) {
            return QString::fromUtf8( "\uf004" );
        }
        if ( iconName == "fa-caret-square-o-right" ) {
            return QString::fromUtf8( "\uf152" );
        }
        if ( iconName == "fa-crosshairs" ) {
            return QString::fromUtf8( "\uf05b" );
        }
        if ( iconName == "fa-arrow-up" ) {
            return QString::fromUtf8( "\uf062" );
        }
        if ( iconName == "fa-info" ) {
            return QString::fromUtf8( "\uf129" );
        }
        if ( iconName == "fa-xing" ) {
            return QString::fromUtf8( "\uf168" );
        }
        if ( iconName == "fa-long-arrow-up" ) {
            return QString::fromUtf8( "\uf176" );
        }
        if ( iconName == "fa-rotate-left" ) {
            return QString::fromUtf8( "\uf0e2" );
        }
        if ( iconName == "fa-sort-down" ) {
            return QString::fromUtf8( "\uf0dd" );
        }
        if ( iconName == "fa-fire" ) {
            return QString::fromUtf8( "\uf06d" );
        }
        if ( iconName == "fa-tasks" ) {
            return QString::fromUtf8( "\uf0ae" );
        }
        if ( iconName == "fa-linkedin-square" ) {
            return QString::fromUtf8( "\uf08c" );
        }
        if ( iconName == "fa-tag" ) {
            return QString::fromUtf8( "\uf02b" );
        }
        if ( iconName == "fa-toggle-on" ) {
            return QString::fromUtf8( "\uf205" );
        }
        if ( iconName == "fa-group" ) {
            return QString::fromUtf8( "\uf0c0" );
        }
        if ( iconName == "fa-instagram" ) {
            return QString::fromUtf8( "\uf16d" );
        }
        if ( iconName == "fa-behance-square" ) {
            return QString::fromUtf8( "\uf1b5" );
        }
        if ( iconName == "fa-cube" ) {
            return QString::fromUtf8( "\uf1b2" );
        }
        if ( iconName == "fa-area-chart" ) {
            return QString::fromUtf8( "\uf1fe" );
        }
        if ( iconName == "fa-scissors" ) {
            return QString::fromUtf8( "\uf0c4" );
        }
        if ( iconName == "fa-archive" ) {
            return QString::fromUtf8( "\uf187" );
        }
        if ( iconName == "fa-pinterest-p" ) {
            return QString::fromUtf8( "\uf231" );
        }
        if ( iconName == "fa-folder" ) {
            return QString::fromUtf8( "\uf07b" );
        }
        if ( iconName == "fa-arrow-left" ) {
            return QString::fromUtf8( "\uf060" );
        }
        if ( iconName == "fa-comment" ) {
            return QString::fromUtf8( "\uf075" );
        }
        if ( iconName == "fa-h-square" ) {
            return QString::fromUtf8( "\uf0fd" );
        }
        if ( iconName == "fa-flag-o" ) {
            return QString::fromUtf8( "\uf11d" );
        }
        if ( iconName == "fa-users" ) {
            return QString::fromUtf8( "\uf0c0" );
        }
        if ( iconName == "fa-gittip" ) {
            return QString::fromUtf8( "\uf184" );
        }
        if ( iconName == "fa-google" ) {
            return QString::fromUtf8( "\uf1a0" );
        }
        if ( iconName == "fa-bitbucket" ) {
            return QString::fromUtf8( "\uf171" );
        }
        if ( iconName == "fa-calendar-o" ) {
            return QString::fromUtf8( "\uf133" );
        }
        if ( iconName == "fa-sort-amount-asc" ) {
            return QString::fromUtf8( "\uf160" );
        }
        if ( iconName == "fa-rebel" ) {
            return QString::fromUtf8( "\uf1d0" );
        }
        if ( iconName == "fa-google-plus" ) {
            return QString::fromUtf8( "\uf0d5" );
        }
        if ( iconName == "fa-th-large" ) {
            return QString::fromUtf8( "\uf009" );
        }
        if ( iconName == "fa-user-plus" ) {
            return QString::fromUtf8( "\uf234" );
        }
        if ( iconName == "fa-stumbleupon" ) {
            return QString::fromUtf8( "\uf1a4" );
        }
        if ( iconName == "fa-globe" ) {
            return QString::fromUtf8( "\uf0ac" );
        }
        if ( iconName == "fa-forward" ) {
            return QString::fromUtf8( "\uf04e" );
        }
        if ( iconName == "fa-sign-in" ) {
            return QString::fromUtf8( "\uf090" );
        }
        if ( iconName == "fa-heart-o" ) {
            return QString::fromUtf8( "\uf08a" );
        }
        if ( iconName == "fa-step-forward" ) {
            return QString::fromUtf8( "\uf051" );
        }
        if ( iconName == "fa-arrows-h" ) {
            return QString::fromUtf8( "\uf07e" );
        }
        if ( iconName == "fa-codepen" ) {
            return QString::fromUtf8( "\uf1cb" );
        }
        if ( iconName == "fa-lightbulb-o" ) {
            return QString::fromUtf8( "\uf0eb" );
        }
        if ( iconName == "fa-youtube" ) {
            return QString::fromUtf8( "\uf167" );
        }
        if ( iconName == "fa-spoon" ) {
            return QString::fromUtf8( "\uf1b1" );
        }
        if ( iconName == "fa-sort-up" ) {
            return QString::fromUtf8( "\uf0de" );
        }
        if ( iconName == "fa-lemon-o" ) {
            return QString::fromUtf8( "\uf094" );
        }
        if ( iconName == "fa-legal" ) {
            return QString::fromUtf8( "\uf0e3" );
        }
        if ( iconName == "fa-paint-brush" ) {
            return QString::fromUtf8( "\uf1fc" );
        }
        if ( iconName == "fa-slack" ) {
            return QString::fromUtf8( "\uf198" );
        }
        if ( iconName == "fa-check-circle" ) {
            return QString::fromUtf8( "\uf058" );
        }
        if ( iconName == "fa-power-off" ) {
            return QString::fromUtf8( "\uf011" );
        }
        if ( iconName == "fa-arrows-v" ) {
            return QString::fromUtf8( "\uf07d" );
        }
        if ( iconName == "fa-mail-forward" ) {
            return QString::fromUtf8( "\uf064" );
        }
        if ( iconName == "fa-eraser" ) {
            return QString::fromUtf8( "\uf12d" );
        }
        if ( iconName == "fa-sliders" ) {
            return QString::fromUtf8( "\uf1de" );
        }
        if ( iconName == "fa-floppy-o" ) {
            return QString::fromUtf8( "\uf0c7" );
        }
        if ( iconName == "fa-pause" ) {
            return QString::fromUtf8( "\uf04c" );
        }
        if ( iconName == "fa-facebook-square" ) {
            return QString::fromUtf8( "\uf082" );
        }
        if ( iconName == "fa-cubes" ) {
            return QString::fromUtf8( "\uf1b3" );
        }
        if ( iconName == "fa-qrcode" ) {
            return QString::fromUtf8( "\uf029" );
        }
        if ( iconName == "fa-fighter-jet" ) {
            return QString::fromUtf8( "\uf0fb" );
        }
        if ( iconName == "fa-ils" ) {
            return QString::fromUtf8( "\uf20b" );
        }
        if ( iconName == "fa-file-text" ) {
            return QString::fromUtf8( "\uf15c" );
        }
        if ( iconName == "fa-dashboard" ) {
            return QString::fromUtf8( "\uf0e4" );
        }
        if ( iconName == "fa-question-circle" ) {
            return QString::fromUtf8( "\uf059" );
        }
        if ( iconName == "fa-file-video-o" ) {
            return QString::fromUtf8( "\uf1c8" );
        }
        if ( iconName == "fa-hand-o-right" ) {
            return QString::fromUtf8( "\uf0a4" );
        }
        if ( iconName == "fa-yelp" ) {
            return QString::fromUtf8( "\uf1e9" );
        }
        if ( iconName == "fa-location-arrow" ) {
            return QString::fromUtf8( "\uf124" );
        }
        if ( iconName == "fa-reply-all" ) {
            return QString::fromUtf8( "\uf122" );
        }
        if ( iconName == "fa-list-ol" ) {
            return QString::fromUtf8( "\uf0cb" );
        }
        if ( iconName == "fa-foursquare" ) {
            return QString::fromUtf8( "\uf180" );
        }
        if ( iconName == "fa-mars-stroke-h" ) {
            return QString::fromUtf8( "\uf22b" );
        }
        if ( iconName == "fa-star-half-o" ) {
            return QString::fromUtf8( "\uf123" );
        }
        if ( iconName == "fa-neuter" ) {
            return QString::fromUtf8( "\uf22c" );
        }
        if ( iconName == "fa-leaf" ) {
            return QString::fromUtf8( "\uf06c" );
        }
        if ( iconName == "fa-question" ) {
            return QString::fromUtf8( "\uf128" );
        }
        if ( iconName == "fa-compass" ) {
            return QString::fromUtf8( "\uf14e" );
        }
        if ( iconName == "fa-shopping-cart" ) {
            return QString::fromUtf8( "\uf07a" );
        }
        if ( iconName == "fa-cloud-upload" ) {
            return QString::fromUtf8( "\uf0ee" );
        }
        if ( iconName == "fa-paperclip" ) {
            return QString::fromUtf8( "\uf0c6" );
        }
        if ( iconName == "fa-remove" ) {
            return QString::fromUtf8( "\uf00d" );
        }
        if ( iconName == "fa-mars-stroke-v" ) {
            return QString::fromUtf8( "\uf22a" );
        }
        if ( iconName == "fa-align-right" ) {
            return QString::fromUtf8( "\uf038" );
        }
        if ( iconName == "fa-plane" ) {
            return QString::fromUtf8( "\uf072" );
        }
        if ( iconName == "fa-git" ) {
            return QString::fromUtf8( "\uf1d3" );
        }
        if ( iconName == "fa-krw" ) {
            return QString::fromUtf8( "\uf159" );
        }
        if ( iconName == "fa-toggle-left" ) {
            return QString::fromUtf8( "\uf191" );
        }
        if ( iconName == "fa-files-o" ) {
            return QString::fromUtf8( "\uf0c5" );
        }
        if ( iconName == "fa-circle" ) {
            return QString::fromUtf8( "\uf111" );
        }
        if ( iconName == "fa-chevron-up" ) {
            return QString::fromUtf8( "\uf077" );
        }
        if ( iconName == "fa-jpy" ) {
            return QString::fromUtf8( "\uf157" );
        }
        if ( iconName == "fa-inr" ) {
            return QString::fromUtf8( "\uf156" );
        }
        if ( iconName == "fa-cab" ) {
            return QString::fromUtf8( "\uf1ba" );
        }
        if ( iconName == "fa-history" ) {
            return QString::fromUtf8( "\uf1da" );
        }
        if ( iconName == "fa-transgender-alt" ) {
            return QString::fromUtf8( "\uf225" );
        }
        if ( iconName == "fa-asterisk" ) {
            return QString::fromUtf8( "\uf069" );
        }
        if ( iconName == "fa-leanpub" ) {
            return QString::fromUtf8( "\uf212" );
        }
        if ( iconName == "fa-bookmark" ) {
            return QString::fromUtf8( "\uf02e" );
        }
        if ( iconName == "fa-tumblr-square" ) {
            return QString::fromUtf8( "\uf174" );
        }
        if ( iconName == "fa-reorder" ) {
            return QString::fromUtf8( "\uf0c9" );
        }
        if ( iconName == "fa-pie-chart" ) {
            return QString::fromUtf8( "\uf200" );
        }
        if ( iconName == "fa-envelope" ) {
            return QString::fromUtf8( "\uf0e0" );
        }
        if ( iconName == "fa-car" ) {
            return QString::fromUtf8( "\uf1b9" );
        }
        if ( iconName == "fa-child" ) {
            return QString::fromUtf8( "\uf1ae" );
        }
        if ( iconName == "fa-caret-square-o-left" ) {
            return QString::fromUtf8( "\uf191" );
        }
        if ( iconName == "fa-table" ) {
            return QString::fromUtf8( "\uf0ce" );
        }
        if ( iconName == "fa-check" ) {
            return QString::fromUtf8( "\uf00c" );
        }
        if ( iconName == "fa-venus-mars" ) {
            return QString::fromUtf8( "\uf228" );
        }
        if ( iconName == "fa-caret-up" ) {
            return QString::fromUtf8( "\uf0d8" );
        }
        if ( iconName == "fa-steam-square" ) {
            return QString::fromUtf8( "\uf1b7" );
        }
        if ( iconName == "fa-unlink" ) {
            return QString::fromUtf8( "\uf127" );
        }
        if ( iconName == "fa-edit" ) {
            return QString::fromUtf8( "\uf044" );
        }
        if ( iconName == "fa-toggle-up" ) {
            return QString::fromUtf8( "\uf151" );
        }
        if ( iconName == "fa-connectdevelop" ) {
            return QString::fromUtf8( "\uf20e" );
        }
        if ( iconName == "fa-meh-o" ) {
            return QString::fromUtf8( "\uf11a" );
        }
        if ( iconName == "fa-level-up" ) {
            return QString::fromUtf8( "\uf148" );
        }
        if ( iconName == "fa-github" ) {
            return QString::fromUtf8( "\uf09b" );
        }
        if ( iconName == "fa-compress" ) {
            return QString::fromUtf8( "\uf066" );
        }
        if ( iconName == "fa-user-md" ) {
            return QString::fromUtf8( "\uf0f0" );
        }
        if ( iconName == "fa-certificate" ) {
            return QString::fromUtf8( "\uf0a3" );
        }
        if ( iconName == "fa-vine" ) {
            return QString::fromUtf8( "\uf1ca" );
        }
        if ( iconName == "fa-sitemap" ) {
            return QString::fromUtf8( "\uf0e8" );
        }
        if ( iconName == "fa-chain-broken" ) {
            return QString::fromUtf8( "\uf127" );
        }
        if ( iconName == "fa-toggle-off" ) {
            return QString::fromUtf8( "\uf204" );
        }
        if ( iconName == "fa-caret-square-o-up" ) {
            return QString::fromUtf8( "\uf151" );
        }
        if ( iconName == "fa-clock-o" ) {
            return QString::fromUtf8( "\uf017" );
        }
        if ( iconName == "fa-file-code-o" ) {
            return QString::fromUtf8( "\uf1c9" );
        }
        if ( iconName == "fa-eyedropper" ) {
            return QString::fromUtf8( "\uf1fb" );
        }
        if ( iconName == "fa-arrow-circle-o-down" ) {
            return QString::fromUtf8( "\uf01a" );
        }
        if ( iconName == "fa-weixin" ) {
            return QString::fromUtf8( "\uf1d7" );
        }
        if ( iconName == "fa-user-times" ) {
            return QString::fromUtf8( "\uf235" );
        }
        if ( iconName == "fa-soccer-ball-o" ) {
            return QString::fromUtf8( "\uf1e3" );
        }
        if ( iconName == "fa-won" ) {
            return QString::fromUtf8( "\uf159" );
        }
        if ( iconName == "fa-home" ) {
            return QString::fromUtf8( "\uf015" );
        }
        if ( iconName == "fa-file-text-o" ) {
            return QString::fromUtf8( "\uf0f6" );
        }
        if ( iconName == "fa-cc-discover" ) {
            return QString::fromUtf8( "\uf1f2" );
        }
        if ( iconName == "fa-gears" ) {
            return QString::fromUtf8( "\uf085" );
        }
        if ( iconName == "fa-ban" ) {
            return QString::fromUtf8( "\uf05e" );
        }
        if ( iconName == "fa-soundcloud" ) {
            return QString::fromUtf8( "\uf1be" );
        }
        if ( iconName == "fa-arrow-circle-o-right" ) {
            return QString::fromUtf8( "\uf18e" );
        }
        if ( iconName == "fa-sort" ) {
            return QString::fromUtf8( "\uf0dc" );
        }
        if ( iconName == "fa-motorcycle" ) {
            return QString::fromUtf8( "\uf21c" );
        }
        if ( iconName == "fa-weibo" ) {
            return QString::fromUtf8( "\uf18a" );
        }
        if ( iconName == "fa-paste" ) {
            return QString::fromUtf8( "\uf0ea" );
        }
        if ( iconName == "fa-hotel" ) {
            return QString::fromUtf8( "\uf236" );
        }
        if ( iconName == "fa-cart-plus" ) {
            return QString::fromUtf8( "\uf217" );
        }
        if ( iconName == "fa-play" ) {
            return QString::fromUtf8( "\uf04b" );
        }
        if ( iconName == "fa-link" ) {
            return QString::fromUtf8( "\uf0c1" );
        }
        if ( iconName == "fa-folder-open" ) {
            return QString::fromUtf8( "\uf07c" );
        }
        if ( iconName == "fa-mobile" ) {
            return QString::fromUtf8( "\uf10b" );
        }
        if ( iconName == "fa-taxi" ) {
            return QString::fromUtf8( "\uf1ba" );
        }
        return QString::fromUtf8( "\uf0a7" );
    }
};

#endif
