#ifndef POSITIONANGLES_H
#define POSITIONANGLES_H

#include <QWidget>
#include <QPainter>

class PositionAngles : public QWidget
{
    Q_OBJECT
public:
    explicit PositionAngles(QWidget *parent = 0);
    void setAngle( int angle );
    void setText( QString text );
protected:
    int angle;
    QString text;
protected:
    void paintEvent(QPaintEvent *);

};

class Compass: public PositionAngles
{
    Q_OBJECT
public:
    explicit Compass(QWidget *parent = 0);
protected:
    void paintEvent(QPaintEvent *);
};

#endif // POSITIONANGLES_H
