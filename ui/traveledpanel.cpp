#include "traveledpanel.h"
#include <QPainter>
#include <QDebug>
#include "ui/FontAwesomeIconNames.h"

TraveledPanel::TraveledPanel(QWidget *parent) : QWidget(parent)
{
    timer = new QTimer;
    connect( timer,SIGNAL(timeout()),this,SLOT(updates()) );

    is_deployed = false;
    color_block.setRgb(72,78,85,200);
    color_text.setRgb(142,150,157);
    setWindowOpacity( 0.0 );
}

void TraveledPanel::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setPen( Qt::NoPen );
    p.setBrush( QBrush( color_block ) );
    p.drawRect(20,0,width()-20,height());

    p.drawRect(0,height()/2,20,30);
    p.setBrush(QBrush(Qt::gray));
    p.drawRect(0,0,20,height()/2);
    p.drawRect(0,height()/2+30,20,height()/2-30);

    p.setFont(QFont("FontAwesome",12));


    p.setPen( Qt::white );

    p.drawText(4,height()/2+20,FontAwesomeIconNames::unicode( "fa-wrench"));

    p.drawText( 30,30, "Настройки отображения");

    p.translate(0,50);

    for( int i = 0; i < list_settings.size(); ++i )
    {
        p.setFont( QFont( "FontAwesome",8 ) );
        p.drawText( 30,33*(i+1), list_settings.at(i).title );
        p.setFont( QFont( "FontAwesome",15 ) );
        p.drawText( 170,33*(i+1),FontAwesomeIconNames::unicode( list_settings.at(i).toogle?"fa-toggle-on":"fa-toggle-off" ) );
    }
}

void TraveledPanel::mousePressEvent(QMouseEvent *event)
{
    int index = ( event->y() - 50 ) / 33;
    if( index >=0 && index < list_settings.size() )
    {
        list_settings[index].set( list_settings[index].toogle?false:true );
        changeState( index,list_settings[index].title,list_settings[index].toogle );
        repaint();
    }
}

void TraveledPanel::enterEvent(QEvent *)
{
    is_deployed = false;
    timer->start( 30 );
}

void TraveledPanel::leaveEvent(QEvent *)
{
    is_deployed = true;
    timer->start( 30 );
}

void TraveledPanel::addSetting(QString title, QString description, bool toogle)
{
    list_settings.push_back( Node( title,toogle,description ) );
}

void TraveledPanel::updates()
{
    int offset = width();
    if( is_deployed )
        offset -= 30;
    else
        offset += 30;

    offset = offset>220?220:offset<0?20:offset;
    setGeometry(this->x()+(width()-offset),this->y(),offset,height() );
    repaint();

    if( width() == 220 || width() == 20 )
        timer->stop();
}

void TraveledPanel::turn()
{
    if( is_deployed )
    {
        is_deployed = false;
        timer->start( 30 );
    }
}

void TraveledPanel::expand()
{
    if( !is_deployed )
    {
        is_deployed = true;
        timer->start( 30 );
    }
}

TraveledPanel::~TraveledPanel()
{
    timer->stop();
    delete( timer );
}
