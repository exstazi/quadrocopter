#include "spatialposition.h"
#include <QDebug>
#include <math.h>

SpatialPosition::SpatialPosition(QWidget *parent) : QWidget(parent)
{
    timer = new QTimer;
    connect( timer,SIGNAL(timeout()),this,SLOT(aa()));
    //timer->start(100);
    roll = 0;
    pitch = 0;
}

void SpatialPosition::changeRoll( int value ) {
    roll = value;
    update();
}

void SpatialPosition::changePitch(int value) {
    pitch = value;
    update();
}

void SpatialPosition::aa() {
    //roll += 10;
    pitch += 5;
    update();
}

void SpatialPosition::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setPen( QPen(Qt::gray,1,Qt::SolidLine) );
    p.setBrush( Qt::gray );
    p.drawRect(0,0,150,150);

    p.translate(75,75);
    p.rotate( roll );
    p.setBrush( QColor(131,81,54) );
    p.drawEllipse( QPoint(0,0),55,55 );
    p.setBrush(Qt::blue);
    p.drawChord(-55,-55,110,110, pitch * 16, (180-2*pitch) * 16);
    p.rotate( -roll );

    p.setPen( QPen(Qt::black,1,Qt::SolidLine) );


    //Постоянные

    p.setBrush( Qt::black );
    QPolygon polygon;
    polygon << QPoint(0,-60) << QPoint( 5,-70) << QPoint( -5,-70 );
    p.drawPolygon(polygon);
    p.setBrush( Qt::black );
    QPolygon polygon3;
    polygon3 << QPoint(-30,-3) << QPoint( -5,-3) << QPoint( -5,5 ) << QPoint(-8,5) << QPoint( -8,0) << QPoint(-30,0);
    p.drawPolygon(polygon3);
    polygon3.clear();
    polygon3 << QPoint(30,-3) << QPoint( 5,-3) << QPoint( 5,5 ) << QPoint(8,5) << QPoint( 8,0) << QPoint(30,0);
    p.drawPolygon(polygon3);

    p.setPen( QPen(Qt::black,1,Qt::SolidLine) );
    p.rotate( 10 );
    p.drawLine( 0,-65,0,-70);
    p.rotate( 10 );
    p.drawLine( 0,-65,0,-70);
    p.rotate( 10 );
    p.drawLine( 0,-65,0,-75);
    p.rotate( 10 );
    p.drawLine( 0,-65,0,-70);
    p.rotate( 10 );
    p.drawLine( 0,-65,0,-70);
    p.rotate( 10 );
    p.drawLine( 0,-65,0,-75);
    p.rotate( -130 );
    p.rotate( 10 );
    p.drawLine( 0,-65,0,-75);
    p.rotate( 10 );
    p.drawLine( 0,-65,0,-70);
    p.rotate( 10 );
    p.drawLine( 0,-65,0,-70);
    p.rotate( 10 );
    p.drawLine( 0,-65,0,-75);
    p.rotate( 10 );
    p.drawLine( 0,-65,0,-70);
    p.rotate( 10 );
    p.drawLine( 0,-65,0,-70);
    p.rotate(10);


    //Поворот тангажа

    //qDebug() << pitch << " " << 50*sin(pitch*0.0174532925);


    //Поворот крена

    p.setPen( QPen(Qt::white,1,Qt::SolidLine) );
    p.setBrush( Qt::white );


    p.rotate( roll );

    //p.drawLine(-55,-55*sin(pitch*0.0174532925),55,-55*sin(pitch*0.0174532925));

    for(int i = 6; i < 98; i+= 18 ) {
        p.drawLine(-15,-55*sin(pitch*0.0174532925)-i,15,-55*sin(pitch*0.0174532925)-i);
        p.drawLine(-15,-55*sin(pitch*0.0174532925)-i-6,15,-55*sin(pitch*0.0174532925)-i-6);
        p.drawLine(-25,-55*sin(pitch*0.0174532925)-i-12,25,-55*sin(pitch*0.0174532925)-i-12);
    }

    for(int i = 6; i < 98; i+= 18 ) {
        p.drawLine(-15,-55*sin(pitch*0.0174532925)+i,15,-55*sin(pitch*0.0174532925)+i);
        p.drawLine(-15,-55*sin(pitch*0.0174532925)+i+6,15,-55*sin(pitch*0.0174532925)+i+6);
        p.drawLine(-25,-55*sin(pitch*0.0174532925)+i+12,25,-55*sin(pitch*0.0174532925)+i+12);
    }





    p.setBrush( Qt::black );
    QPolygon polygon2;
    polygon2 << QPoint(0,-60) << QPoint( 5,-50) << QPoint( -5,-50 );
    p.drawPolygon(polygon2);



}

SpatialPosition::~SpatialPosition()
{

}

