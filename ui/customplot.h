#ifndef CUSTOMPLOT_H
#define CUSTOMPLOT_H

#include <QWidget>

class CustomPlot : public QWidget
{
    Q_OBJECT
public:
    explicit CustomPlot(QWidget *parent = 0);
    ~CustomPlot();

signals:

public slots:
};

#endif // CUSTOMPLOT_H
