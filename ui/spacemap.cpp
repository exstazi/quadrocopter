#include "spacemap.h"
#include <QDebug>
#include <QTime>

SpaceMap::SpaceMap(QWidget *parent) : QWidget(parent)
{
    setSizeMap(10,10,2);

    qsrand( QTime(0,0,0).secsTo(QTime::currentTime()));
    select_level_z = 0;
    is_all_append = false;

    color_active = Qt::blue;
    color_not_active = Qt::gray;
    color_rect_border = Qt::white;
}

void SpaceMap::setSizeMap(int x, int y, int z)
{
    if( count_x == x && count_y == y && count_z == z )
    {
        return ;
    }
    count_x = x;
    count_y = y;
    count_z = z;

    space = new bool**[count_x];
    for( int i = 0; i < count_x; ++i )
    {
        space[i] = new bool*[count_y];
        for( int j = 0; j < count_y; ++j )
        {
            space[i][j] = new bool[count_z];
            for( int k = 0; k < count_z; ++k )
            {
                space[i][j][k] = false;
            }
        }
    }
    //resizeEvent(NULL);
}

void SpaceMap::clear()
{
    for( int i = 0; i < count_x; ++i )
    {
        for( int j = 0; j < count_y; ++j )
        {
            for( int k = 0; k < count_z; ++k )
            {
                space[i][j][k] = false;
            }
        }
    }
    repaint();
}

void SpaceMap::generateRandom()
{
    int count_block = qrand() % (count_x*count_y*count_z);

    while( count_block-- )
    {
        space[qrand() % count_x][qrand() % count_y][qrand() % count_z] = true;
    }
    repaint();
}

void SpaceMap::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setPen( Qt::white );

    for( int i = 0; i < count_x; ++i )
    {
        for( int j = 0; j < count_y; ++j )
        {
            if( space[i][j][select_level_z] )
                p.setBrush( QBrush( color_active ) );
            else
                p.setBrush( QBrush( color_not_active ) );
            p.drawRect( offset_x + size_cell*i, offset_y + size_cell*j,size_cell,size_cell );
        }
    }
    p.setPen( Qt::black );
    p.drawText( 30,30,QString( "Level " + QString::number(select_level_z) ) );

    p.setBrush( QBrush( QColor( 50,64,78 ) ) );
    p.setPen( Qt::white );
    p.drawRect( 20,50,90,20 );
    p.drawText( 30,65,QString( "Очистить" ) );

    p.drawRect( 20,80,90,20 );
    p.drawText( 30,95,QString( "Генерировать" ) );
}

void SpaceMap::mouseMoveEvent(QMouseEvent *event)
{
    int i = ( event->x() - offset_x ) / size_cell;
    int j = ( event->y() - offset_y ) / size_cell;
    if( !( i < 0 || i > count_x || j < 0 || j > count_y ) && space[i][j][select_level_z] != mouse_move_append  )
    {
        space[i][j][select_level_z] = mouse_move_append;
        if( is_all_append )
        {
            for( int k = 0; k < count_z; ++k )
            {
                space[i][j][k] = mouse_move_append;
            }
        }
        repaint();
    }
}

void SpaceMap::mousePressEvent(QMouseEvent *event)
{
    if( event->x() > 20 && event->x() < 100 && event->y() > 50 && event->y() < 70 )
    {
        clear();
        return ;
    }

    if( event->x() > 20 && event->x() < 100 && event->y() > 80 && event->y() < 100 )
    {
        generateRandom();
        return ;
    }

    if( event->button() == Qt::LeftButton )
        is_all_append = false;
    else
        is_all_append = true;
    int i = ( event->x() - offset_x ) / size_cell;
    int j = ( event->y() - offset_y ) / size_cell;
    if( !( i < 0 || i > count_x || j < 0 || j > count_y ) )
    {
        space[i][j][select_level_z] = space[i][j][select_level_z]?false:true;
        mouse_move_append = space[i][j][select_level_z];
        if( is_all_append )
        {
            for( int k = 0; k < count_z; ++k )
            {
                space[i][j][k] = mouse_move_append;
            }
        }

        repaint();
    }
}

void SpaceMap::wheelEvent(QWheelEvent *event)
{
    if( event->delta() > 0 )
    {
        ++select_level_z;
    }
    else
    {
        --select_level_z;
    }
    select_level_z = select_level_z>count_z-1?count_z-1:select_level_z<0?0:select_level_z;
    repaint();
}

void SpaceMap::resizeEvent(QResizeEvent *)
{
    int a = width() / count_x;
    size_cell = height() / count_y;
    if( a < size_cell )
        size_cell = a;
    if( size_cell > 50 )
        size_cell = 50;
    offset_x = ( width() - size_cell*count_x ) / 2;
    offset_y = ( height() - size_cell*count_y ) / 2;
}

SpaceMap::~SpaceMap()
{
    for( int i = 0; i < count_x; ++i )
    {
        for( int j = 0; j < count_y; ++j )
        {
            delete space[i][j];
        }
        delete space[i];
    }
    delete space;
}

