#ifndef SPACEMAP_H
#define SPACEMAP_H

#include <QWidget>
#include <QPainter>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QResizeEvent>
#include <QWheelEvent>

class SpaceMap : public QWidget
{
    Q_OBJECT

protected:
    int count_x;
    int count_y;
    int count_z;
public:
    bool ***space;
protected:
    bool mouse_move_append;
    bool is_all_append;
    int select_level_z;

    int size_cell;
    int offset_x;
    int offset_y;

    QColor color_active;
    QColor color_not_active;
    QColor color_rect_border;

public:
    explicit SpaceMap(QWidget *parent = 0);
    void setSizeMap( int x,int y,int z );
    void clear();
    void generateRandom();
    ~SpaceMap();


protected:
    void paintEvent(QPaintEvent*);
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void wheelEvent(QWheelEvent* event);
    void resizeEvent(QResizeEvent*);

};

#endif // SPACEMAP_H
