#include "navigation.h"
#include <QDebug>
#include <QPainter>
#include <QFontMetrics>
#include "FontAwesomeIconNames.h"

Navigation::Navigation(QWidget *parent) : QWidget(parent)
{
    setMouseTracking(true);

    active_index = select_index = -1;
    is_full = false;

    timer = new QTimer;
    connect( timer,SIGNAL(timeout()),this,SLOT(updates()) );

    color_block.setRgb(50,64,78);
    color_active_block.setRgb(44,56,69);
    color_text.setRgb(142,150,157);
    color_active_text = Qt::white;
    color_left_rect.setRgb(41,134,184);

    width_minimaze = 50;
    width_maximaze = 200;
    height_button = 33;

    speed_scroll = 30;

    logo = new QPixmap( ":/img/logo" );
}

void Navigation::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setPen( Qt::NoPen );
    p.setBrush((QBrush(color_block)));
    p.drawRect(0,0,width(),height());
    p.drawPixmap(0,0,*logo);
    p.translate(0,logo->height());

    if( !is_full )
    {
        p.setFont( QFont("FontAwesome",11) );
    }

    for( int i = 0; i < list_button.size(); ++i )
    {
        if( i == select_index ) continue;

        if( i == active_index )
        {
            p.setPen( Qt::NoPen );
            p.setBrush( QBrush(color_active_block));
            p.drawRect( 0,height_button*i,width(),height_button);
            p.setPen( color_active_text);
        }
        else
        {
            p.setPen( color_text );
        }
        p.drawText( 18,height_button*i+23,list_button.at(i).get( is_full ) );
    }

    if( select_index >= 0 && select_index < list_button.size() )
    {
        p.setPen(Qt::NoPen);
        if( is_full )
        {
            p.setBrush(QBrush(color_active_block));
            p.drawRect(0,height_button*select_index,width(),height_button);
            p.setBrush(QBrush(color_left_rect));
            p.drawRect(0,height_button*select_index,3,height_button);
        }
        else
        {
            p.setBrush(QBrush(color_left_rect));
            p.drawRect(0,height_button*select_index,width(),height_button);
        }

        p.setPen( Qt::white );
        p.drawText( is_full?25:18,height_button*select_index+23,list_button.at(select_index).get( is_full ));
    }

    p.setPen(Qt::NoPen);
    p.setBrush(QBrush( color_active_block ) );
    p.drawRect(0,height()-73,width(),23);
    p.setPen( color_active_text );
    p.setBrush( Qt::NoBrush );
    p.setFont(QFont("FontAwesome",10));

    p.drawText( width()-20, height()-56, FontAwesomeIconNames::unicode( is_full?"fa-angle-double-left":"fa-angle-double-right" ) );

}

void Navigation::updates()
{
    int offset = width();
    if( is_full )
        offset += speed_scroll;
    else
        offset -= speed_scroll;

    setFixedWidth( offset>width_maximaze?width_maximaze:offset<width_minimaze?width_minimaze:offset );
    repaint();

    if( width() == width_maximaze || width() == width_minimaze )
        timer->stop();
}

void Navigation::addButton(QString name,QString icon)
{
    list_button.append( Button( name,FontAwesomeIconNames::unicode( icon ) ) );
}

void Navigation::setMinimaze()
{
    if( is_full )
    {
        is_full = false;
        timer->start( 30 );
    }
}

void Navigation::setMaximaze()
{
    if( !is_full )
    {
        is_full = true;
        timer->start( 30 );
    }
}

void Navigation::mouseMoveEvent(QMouseEvent *event)
{
    int index = (event->y()-50) / 33;
    if( index != active_index && index < list_button.size() )
    {
        active_index = index;
        //emit clicked( active_index,list_button.at(active_index).name );
        if( !is_full )
        {
            tool_tip->setText( list_button.at(active_index).name );
            int widt2h = QFontMetrics( QFont("FontAwesome",8 ) ).width( list_button.at(active_index).name );
            tool_tip->setGeometry( 50,index*33+50,widt2h + 20,33);
            tool_tip->setVisible( true );
            if( index == select_index )
                tool_tip->setColor( color_left_rect );
            else
                tool_tip->setColor( color_active_block);
        }
        this->repaint();
    } else if( index != active_index ) {
        active_index = -1;
        tool_tip->setVisible( false );
        this->repaint();
    }
}

void Navigation::leaveEvent(QEvent*)
{
    active_index = -1;
    tool_tip->setVisible(false);
    repaint();
    emit clicked(-1,"das");
}

void Navigation::mousePressEvent(QMouseEvent *event)
{
    if( event->y() > height()-23)
    {
        if( is_full)
            setMinimaze();
        else
            setMaximaze();
        return ;
    }
    if( active_index != select_index )
    {
        select_index = active_index;
        tool_tip->setColor( color_left_rect );
        repaint();
        emit clicked( select_index );
    }
}

Navigation::~Navigation()
{
    list_button.clear();
    delete( timer );
}
