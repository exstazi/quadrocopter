#include "hint.h"

Hint::Hint(QWidget *parent) : QWidget(parent)
{

}

void Hint::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setPen( Qt::NoPen );
    p.setBrush((QBrush(color)));
    p.drawRect(0,0,width(),height());
    p.setPen( Qt::white );
    p.drawText( 5,20,text );
}


Hint::~Hint()
{

}

