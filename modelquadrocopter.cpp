#include "modelquadrocopter.h"
#include <QDebug>
#include <QGLWidget>

ModelQuadroCopter::ModelQuadroCopter()
{
    obj_model = new ObjReader( ":models/quadrocopter" );
    obj_model->readFile();
    createArray();
    speed[0] = speed[1] = speed[2] = speed[3] = 0;
}

void ModelQuadroCopter::createArray()
{
     QVector<CVector3<float> > v = obj_model->getV( "Cylinder05" );
     QVector<CVector3<int> > f = obj_model->getF( "Cylinder05" );
     body = new CVector3<float>[f.size()*3];
     size_body = f.size()*3;
     int j = 0;
     for( int i = 0; i < f.size(); ++i ) {
        body[j++] =  CVector3<float>( v.at( f.at(i).x - 1 ).x, v.at( f.at(i).x - 1 ).y, v.at( f.at(i).x - 1 ).z );
        body[j++] =  CVector3<float>( v.at( f.at(i).y - 1 ).x, v.at( f.at(i).y - 1 ).y, v.at( f.at(i).y - 1 ).z );
        body[j++] =  CVector3<float>( v.at( f.at(i).z - 1 ).x, v.at( f.at(i).z - 1 ).y, v.at( f.at(i).z - 1 ).z );
     }

     QVector<CVector3<float> > v1 = obj_model->getV( "Box06" );
     QVector<CVector3<int> > f1 = obj_model->getF( "Box06" );

     size_propeller = f1.size()*3;

     propeller = new CVector3<float>[size_propeller];
     j = 0;
     for( int i = 0; i < f1.size(); ++i ) {
        propeller[j++] =  CVector3<float>( v1.at( f1.at(i).x - v.size() - 1 ).x - 0.27, v1.at( f1.at(i).x - v.size() - 1 ).y - 0.27, v1.at( f1.at(i).x - v.size() - 1 ).z );
        propeller[j++] =  CVector3<float>( v1.at( f1.at(i).y - v.size() - 1 ).x - 0.27, v1.at( f1.at(i).y - v.size() - 1 ).y - 0.27, v1.at( f1.at(i).y - v.size() - 1 ).z );
        propeller[j++] =  CVector3<float>( v1.at( f1.at(i).z - v.size() - 1 ).x - 0.27, v1.at( f1.at(i).z - v.size() - 1 ).y - 0.27, v1.at( f1.at(i).z - v.size() - 1 ).z );
     }

}

void ModelQuadroCopter::print( float x,float y,float z,int roll,int pitch,int yaw )
{
    glTranslatef( x, y, z );

    glRotatef(roll, 1.0f, 0.0f, 0.0f);
    glRotatef(pitch, 0.0f, 1.0f, 0.0f);
    glRotatef(yaw, 0.0f, 0.0f, 1.0f);

    speed[0] += front_l;
    speed[1] += front_r;
    speed[2] += rear_l;
    speed[3] += rear_r;

    glColor3f (0.0, 1.0, 0.0);

    glTranslatef( 0.27,0.27,0 );
    glRotatef( speed[0],0.0f,0.0f,1.0f );
    glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3,GL_FLOAT,sizeof(CVector3<float>),propeller);
        glDrawArrays(GL_TRIANGLES,0,size_propeller);
    glDisableClientState(GL_VERTEX_ARRAY);

    glRotatef( -speed[0],0.0f,0.0f,1.0f );

    glTranslatef( 0,-0.54,0 );
    glRotatef( -speed[1],0.0f,0.0f,1.0f );
    glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3,GL_FLOAT,sizeof(CVector3<float>),propeller);
        glDrawArrays(GL_TRIANGLES,0,size_propeller);
    glDisableClientState(GL_VERTEX_ARRAY);



    glRotatef( speed[1],0.0f,0.0f,1.0f );

    glColor3f (0.0, 0.0, 0.0);

    glTranslatef( -0.54,0,0 );
    glRotatef( speed[2],0.0f,0.0f,1.0f );
    glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3,GL_FLOAT,sizeof(CVector3<float>),propeller);
        glDrawArrays(GL_TRIANGLES,0,size_propeller);
    glDisableClientState(GL_VERTEX_ARRAY);

    glRotatef( -speed[2],0.0f,0.0f,1.0f );


    glTranslatef( 0,0.54,0 );
    glRotatef( -speed[3],0.0f,0.0f,1.0f );
    glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3,GL_FLOAT,sizeof(CVector3<float>),propeller);
        glDrawArrays(GL_TRIANGLES,0,size_propeller);
    glDisableClientState(GL_VERTEX_ARRAY);

    glRotatef( speed[3],0.0f,0.0f,1.0f );
    glTranslatef( 0.27,-0.27,0 );

    glColor3f (1.0, 1.0, 1.0);
    glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3,GL_FLOAT,sizeof(CVector3<float>),body);
        glDrawArrays(GL_TRIANGLES,0,size_body);
    glDisableClientState(GL_VERTEX_ARRAY);

    glColor3f (1.0, 1.0, 1.0);
}

ModelQuadroCopter::~ModelQuadroCopter()
{
    delete obj_model;
}

