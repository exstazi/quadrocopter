#include "sceneworld.h"
#include <QWheelEvent>

SceneWorld::SceneWorld(QWidget *parent):
    QGLWidget(parent)
{
    scale = 0.3f;
    rotate_x = -90; rotate_y = rotate_z = 0;
    setAttribute(Qt::WA_OpaquePaintEvent);
}

void SceneWorld::initializeGL() {
    qglClearColor( Qt::gray );
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    // Сглаживание точек
    glEnable(GL_POINT_SMOOTH);
    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
    // Сглаживание линий
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    // Сглаживание полигонов
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
}

void SceneWorld::resizeGL(int nWidth, int nHeight ){
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    GLfloat ratio=(GLfloat)nHeight/(GLfloat)nWidth;

    if (nWidth>=nHeight) {
        glOrtho(-1.0/ratio, 1.0/ratio, -1.0, 1.0, -10.0, 1.0);
    } else {
        glOrtho(-1.0, 1.0, -1.0*ratio, 1.0*ratio, -10.0, 1.0);
    }

    glViewport(0, 0, (GLint)nWidth, (GLint)nHeight);
}

void SceneWorld::wheelEvent(QWheelEvent *pe) {
    if( pe->delta() > 0 ) {
        scale *= 1.1;
    } else {
        scale /= 1.1;
    }
    //updateGL();
}

void SceneWorld::mousePressEvent(QMouseEvent* pe) {
    ptrMousePosition = pe->pos();
}

void SceneWorld::mouseMoveEvent(QMouseEvent* pe) {
    rotate_x += 180/* /scale */ * (GLfloat)(pe->y()-ptrMousePosition.y())/height();
    rotate_z += 180/* /scale */ * (GLfloat)(pe->x()-ptrMousePosition.x())/width();
    ptrMousePosition = pe->pos();
    //updateGL();
}
