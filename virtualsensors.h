#ifndef VIRTUALSENSORS_H
#define VIRTUALSENSORS_H

#include "imemssensors.h"
#include "copter.h"
#include "sceneworld.h"

class VirtualSensors: public IMemsSensors
{
    Q_OBJECT
private:
    Copter * copter;
    SceneWorld * scene;
public:
    VirtualSensors( Copter *copter,SceneWorld *scene = NULL );
    ~VirtualSensors();
    void setWorld( SceneWorld *scene ){ this->scene = scene; }
private slots:
    void changePwm( int front_l,int front_r,int rear_l,int rear_r );
    void update();
};

#endif // VIRTUALSENSORS_H
