#ifndef MODELDEFAULT_H
#define MODELDEFAULT_H

#include "modelcopter.h"

class ModelDefault: public ModelCopter
{
public:
    ModelDefault();
    ~ModelDefault();

    void print(float x,float y,float z,int roll,int pitch,int yaw);
};

#endif // MODELDEFAULT_H
