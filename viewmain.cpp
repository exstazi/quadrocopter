#include "viewmain.h"
#include "ui_viewmain.h"
#include "ui/FontAwesomeIconNames.h"
#include <QDebug>

#include "copter.h"
#include "realequimped.h"
#include "service/comport.h"
#include "virtualsensors.h"
#include "ui/spacemap.h"
#include "keybordequimped.h"
#include "dijkstra.h"

ViewMain::ViewMain(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ViewMain)
{
    ui->setupUi(this);

    green_color = "<font color='green'>%1</font>";
    red_color = "<font color='red'>%1</font>";
    blue_color = "<font color='blue'>%1</font>";

    setMouseTracking( true );

    ui->widget->addButton( "Виртуальный мир","fa-plane" );
    ui->widget->addButton( "Добавление квадрокоптера","fa-plus" );
    ui->widget->addButton( "Создание препятсвий","fa-cubes");
    ui->widget->addButton( "Графическое отображение","fa-area-chart");
    ui->widget->addButton( "Настройки","fa-cog" );
    ui->widget->addButton( "Wifi","fa-wifi" );
    ui->widget->addButton( "Com порт","fa-cog" );

    ui->right_panel->addSetting( "Показывать сетку", "Сетка отображает созданное пространство.",true );
    ui->right_panel->addSetting( "Показывать плоскости", "Отображется 3 плоскости. Накладываются на сетку." );
    ui->right_panel->addSetting( "Стики геймпада","",true );
    ui->right_panel->addSetting( "Навигация","",true );
    ui->right_panel->addSetting( "ШИМ моторов","" );

    //scene = new VirtualWorld( 10,10,3,this );
    //scene->setGeometry(200,50,700,500);

    //ui->title_and_context_layout->addWidget( scene );

    connect( ui->right_panel,SIGNAL(changeState(int,QString,bool)),this,SLOT(changeToggle(int,QString,bool)) );

    ui->widget->setHint( ui->tooltip );

    ui->horizontalWidget_2->setVisible( false );
    //ui->widget_3->setVisible( false );

    connect( ui->widget,SIGNAL(clicked(int)),this,SLOT(nav_clk(int)) );


    //connect( ui->btn,SIGNAL(clicked()),this,SLOT(btn_clk()) );

    //////////
    ui->tooltip->setVisible( false );
    /////////


    map = new SpaceMap( this );
    world = new VirtualWorld( this );

    alg = new Dijkstra( world );

    ui->widget_2->insertWidget(0,world);
    ui->widget_2->insertWidget(2,map );
    ui->widget_2->setCurrentIndex(1);

    connect( ui->btn_add_copter,SIGNAL(clicked()),this,SLOT(btnAddCopter()) );
    connect( ui->btn_edit_param_world,SIGNAL(clicked()),this,SLOT(btnEditVirtualWolrd()) );
    connect( ui->btn_connect_com_port,SIGNAL(clicked()),this,SLOT(btnConnectToComPort()) );
    connect( ui->btn_send_data_to_com_port,SIGNAL(clicked()),this,SLOT(btnSendDataToComPort()) );
    connect( ui->btn_clear_terminal_com_port,SIGNAL(clicked()),this,SLOT(btnClearTerminalComPort()) );
}

void ViewMain::nav_clk(int index)
{
    switch( index )
    {
    case 0:
        world->setObstacle( map->space );
        alg->setObstacle( map->space );
        break;

    case 2:
        map->setSizeMap( world->length,world->width,world->height );
        break;
    case 6:
        ui->list_com_port->clear();
        foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
        {
            if( !info.isBusy() )
            {
                ui->list_com_port->addItem(info.portName());
            }
        }
        break;
    }
    ui->widget_2->setCurrentIndex( index );
}

void ViewMain::changeToggle(int index, QString title, bool toggle)
{
    switch( index )
    {
    case 2:
        ui->stick_r_2->setVisible( toggle );
        ui->stick_l_2->setVisible( toggle );
        break;
    case 3:
        ui->widget_3->setVisible( toggle );
        break;
    }
}

void ViewMain::btnAddCopter()
{
    //QList<QSerialPortInfo> list = QSerialPortInfo::availablePorts();

    Copter *new_copter = new Copter( alg );
    VirtualSensors *sensors = new VirtualSensors( new_copter,world );
    new_copter->setSensors( sensors );

    world->addCopter( new_copter );
}

void ViewMain::btnEditVirtualWolrd()
{
    if( ui->line_length->text().size() != 0 &&
            ui->line_width->text().size() != 0 &&
            ui->line_height->text().size() != 0 )
    {
        world->setSizeWorld( ui->line_length->text().toInt(),
                             ui->line_width->text().toInt(),
                             ui->line_height->text().toInt());

        map->setSizeMap(  ui->line_length->text().toInt(),
                          ui->line_width->text().toInt(),
                          ui->line_height->text().toInt() );
    }

    if( ui->line_fps->text().size() != 0 )
    {
        world->setFps( ui->line_fps->text().toInt() );
    }
}

void ViewMain::btnConnectToComPort()
{
    if( ui->list_com_port->count() != 0 )
    {
        ui->terminal_com_ports->appendPlainText( "Подключение к " + ui->list_com_port->currentText() + "..." );
        port = new QSerialPort( ui->list_com_port->currentText() );
        if ( port->open( QIODevice::ReadWrite ) ) {
            if ( port->setBaudRate( ui->list_baud_rate->currentText().toInt() )
                    && port->setDataBits( (QSerialPort::DataBits)ui->list_data_bits->currentText().toInt() )
                    && port->setParity( QSerialPort::NoParity )
                    && port->setStopBits( (QSerialPort::StopBits)ui->list_stop_bits->currentText().toInt() )
                    && port->setFlowControl( QSerialPort::NoFlowControl ) )
            {
                if ( port->isOpen() ){
                    ui->terminal_com_ports->appendHtml( green_color.arg( "Подключено к " + port->portName() ) );
                    ui->status_connect_to_com_port->setText( "Подключенo к " + port->portName() );

                    connect( port,SIGNAL(readyRead()),this,SLOT(comPortDataReady()) );
                    return ;
                }
            }
        }
        port->close();
        ui->terminal_com_ports->appendHtml( red_color.arg( "Ошибка: " + port->errorString() ) );
    }
}

void ViewMain::comPortDataReady()
{
    ui->terminal_com_ports->appendHtml( blue_color.arg( port->readAll().toStdString().c_str() ) );
}

void ViewMain::btnSendDataToComPort()
{
    if( ui->line_data_com_port->text().size() > 0 )
    {
        port->write( ui->line_data_com_port->text().toStdString().c_str(),
                     ui->line_data_com_port->text().size() );
    }
}

void ViewMain::btnClearTerminalComPort()
{
    ui->terminal_com_ports->clear();
}

void ViewMain::btn_clk()
{
    if( ui->right_panel->isDeployed() )
    {
        ui->right_panel->turn();
    }
    else
    {
        ui->right_panel->expand();
    }
}

void ViewMain::resizeEvent(QResizeEvent *)
{
    ui->horizontalLayoutWidget->setGeometry(0,0,width(),height() );
    ui->right_panel->setGeometry( width()-20,50,20,height() - 50 );
    //ui->horizontalLayoutWidget_2->setGeometry(50,height()-80,width()-50,80 );
}

void ViewMain::mouseMoveEvent(QMouseEvent *)
{
    //qDebug() << event->x() << " " << event->y();
}

ViewMain::~ViewMain()
{
    delete ui;
}
