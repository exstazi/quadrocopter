#-------------------------------------------------
#
# Project created by QtCreator 2015-02-10T00:02:12
#
#-------------------------------------------------

QT       += core gui
QT       += serialport
QT       += opengl
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QuadroCopter
TEMPLATE = app


SOURCES += main.cpp\
        viewapp.cpp \
    service/comport.cpp \
    realequimped.cpp \
    virtualsensors.cpp \
    realsensors.cpp \
    realrawsensors.cpp \
    copter.cpp \
    modelquadrocopter.cpp \
    service/objreader.cpp \
    modeldefault.cpp \
    virtualworld.cpp \
    sceneworld.cpp \
    pid.cpp \
    ui/spatialposition.cpp \
    ui/stick.cpp \
    ui/progressbar.cpp \
    keybordequimped.cpp \
    settings.cpp \
    viewcommunicate.cpp \
    ui/positionangles.cpp \
    ui/navigation.cpp \
    viewmain.cpp \
    ui/traveledpanel.cpp \
    ui/hint.cpp \
    ui/tooltips.cpp \
    ui/spacemap.cpp \
    ui/customplot.cpp \
    dijkstra.cpp

HEADERS  += viewapp.h \
    service/comport.h \
    service/icommunicate.h \
    iradioequimped.h \
    realequimped.h \
    imemssensors.h \
    virtualsensors.h \
    realsensors.h \
    realrawsensors.h \
    copter.h \
    modelcopter.h \
    modelquadrocopter.h \
    service/objreader.h \
    modeldefault.h \
    sceneworld.h \
    virtualworld.h \
    pid.h \
    ui/spatialposition.h \
    ui/stick.h \
    ui/progressbar.h \
    keybordequimped.h \
    settings.h \
    viewcommunicate.h \
    ui/positionangles.h \
    ui/FontAwesomeIconNames.h \
    ui/navigation.h \
    viewmain.h \
    ui/traveledpanel.h \
    ui/hint.h \
    ui/tooltips.h \
    ui/spacemap.h \
    ui/customplot.h \
    dijkstra.h

FORMS    += viewapp.ui \
    settings.ui \
    viewcommunicate.ui \
    viewmain.ui

RESOURCES += \
    resource.qrc
