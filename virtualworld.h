#ifndef VIRTUALWORLD_H
#define VIRTUALWORLD_H

#include "sceneworld.h"

class VirtualWorld: public SceneWorld
{
    Q_OBJECT
public:
    uint length;
    uint width;
    uint height;
private:
    int ***world;

    bool mode_view_grid;
    bool mode_view_plane;

    uint fps;

protected:
    void paintGL();

    void printGrid();
    void printPlane();

    void printWorld();
    void generateWorld();
    //void keyPressEvent(QKeyEvent *pe);
public:
    explicit VirtualWorld( QWidget *parent = 0 );
    VirtualWorld(uint l, uint w, uint h , QWidget *parent = 0);
    ~VirtualWorld();

    void setViewGrid( bool mode );
    void setViewPlane( bool mode );

    float* checkPosistion(float x, float y, float z);

    void setObstacle( bool ***barrier );

    void setSizeWorld( uint legnth,uint width,uint height );
    void setFps(uint fps);
};

#endif // VIRTUALWORLD_H
