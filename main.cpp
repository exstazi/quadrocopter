#include "viewapp.h"
#include "viewmain.h"
#include <QApplication>
#include <QFontDatabase>
#include <QDebug>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFontDatabase::addApplicationFont( ":/fonts/fontawesone" );

    ViewMain w;
    //w.setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnBottomHint);
    w.show();
    return a.exec();
}
