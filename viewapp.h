#ifndef VIEWAPP_H
#define VIEWAPP_H

#include <QMainWindow>
#include <QSerialPort>
#include "service/comport.h"
#include "realequimped.h"

#include "sceneworld.h"
#include "virtualworld.h"

namespace Ui {
class ViewApp;
}

class ViewApp : public QMainWindow
{
    Q_OBJECT

public:
    explicit ViewApp(QWidget *parent = 0);
    ~ViewApp();
protected:
    void resizeEvent(QResizeEvent *event);
private:
    Ui::ViewApp *ui;
    SceneWorld *scene;
    IRadioEquimped *a;

    QSerialPort *port;
signals:
    void keyPress( int key );
private slots:
    void changedPwm( int a,int b,int c,int d );
    void keyPressEvent(QKeyEvent*);
    void btn_nav_click();
    void connect_wifi();
    void connect_com_port();

    void update_list_wifi_networks();
    void update_list_com_ports();
    void on_tabWidget_tabBarClicked(int index);
};


#endif // VIEWAPP_H
