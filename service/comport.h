#ifndef COMPORT_H
#define COMPORT_H

#include <QSerialPort>
#include "icommunicate.h"

class ComPort: public ICommunicate
{
    Q_OBJECT
private:
    QSerialPort port;

    QString port_name;
    QSerialPort::BaudRate baud_rate;
    QSerialPort::DataBits data_bits;
    QSerialPort::Parity parity;
    QSerialPort::StopBits stop_bits;
    QSerialPort::FlowControl flow_control;

    QByteArray buffer;

public:
    ComPort(QString port_name, int baud_rate = QSerialPort::Baud9600,
            int data_bits = QSerialPort::Data8, int parity = QSerialPort::NoParity,
            int stop_bits = QSerialPort::OneStop, int flow_control = QSerialPort::NoFlowControl);
    bool communicate();
    ~ComPort();

public slots:
    void readyComPort();
};

#endif // COMPORT_H
