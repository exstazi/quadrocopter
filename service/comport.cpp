#include "comport.h"
#include <QDebug>

ComPort::ComPort( QString port_name, int baud_rate, int data_bits, int parity, int stop_bits, int flow_control )
{
    this->port_name = port_name;
    this->baud_rate = QSerialPort::Baud9600;
    this->data_bits = (QSerialPort::DataBits)data_bits;
    this->parity = (QSerialPort::Parity)parity;
    this->stop_bits = (QSerialPort::StopBits)stop_bits;
    this->flow_control = (QSerialPort::FlowControl)flow_control;

    connect( &port,SIGNAL(readyRead()),this,SLOT(readyComPort()) );
}

bool ComPort::communicate()
{
    port.setPortName( port_name );
    if ( port.open( QIODevice::ReadWrite ) ) {
        if ( port.setBaudRate( baud_rate )
                && port.setDataBits( data_bits )
                && port.setParity( parity )
                && port.setStopBits( stop_bits )
                && port.setFlowControl( flow_control ) )
        {
            if ( port.isOpen() ){
                qDebug() << "port open";
                return true;
            }
        }
    }
    port.close();
    status = port.errorString();
    return false;
}

void ComPort::readyComPort()
{
    buffer.append( port.readAll() );

    int command;
    while( buffer.size() > 2 ) {
        command = buffer.at(0) & 0x1F;
        if( command >= 0 && command < 4 ) {
            int temp = buffer.at(1);
            emit readyData( command,temp );
            buffer.remove(0,2);
            qDebug() << command << " " << temp;
        } else {
            buffer.remove(0,1);
        }
    }
}

ComPort::~ComPort()
{
    port.close();
    buffer.clear();
}

