#include "objreader.h"
#include <QDebug>
#include <QList>

ObjReader::ObjReader( QString name_files )
{
    file = new QFile( name_files );
}

void ObjReader::readFile()
{
    if( !file->open( QIODevice::ReadOnly ) ) {
        qDebug() << "File not open!";
        return ;
    }

    QString currKey;
    QList<QByteArray> buf;


    while( !file->atEnd() ) {
        buf = file->readLine().split( ' ' );
        if( buf.at(0) == "v" ) {
            models[currKey]->v.append( CVector3<float>( buf.at(1).toFloat() / 150,
                                                        buf.at(2).toFloat() / 150,
                                                        buf.at(3).left(buf.at(3).size()-2).toFloat() / 150 ) );
        } else if( buf.at(0) == "f" ) {
            models[currKey]->f.append( CVector3<int>( buf.at(1).split( '/' ).at(0).toInt(),
                                                      buf.at(2).split( '/' ).at(0).toInt(),
                                                      buf.at(3).split( '/' ).at(0).toInt()) );
        } /*else if( buf.at(0) == "vt" ) {
            models[currKey]->vt.append( CVector3<float>( buf.at(1).toFloat(),
                                                         buf.at(2).left(buf.at(2).size()-2).toFloat(),0.0 ) );
        } */ else if( buf.at(0) == "g" ) {
            currKey = buf.at(1).left( buf.at(1).size() - 2 );
            //qDebug() << currKey;
            models.insert( currKey,new NodeModel );
        }
    }
}

ObjReader::~ObjReader()
{
    models.clear();
    delete file;
}

