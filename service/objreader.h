#ifndef OBJREADER_H
#define OBJREADER_H

#include <QHash>
#include <QVector>
#include <QStringList>
#include <QString>
#include <QFile>

template <typename T>
struct CVector3 {
    T x,y,z;
    CVector3(){ x = y = z = 0;}
    CVector3( T x1,T y1,T z1 ) {
        x = x1;
        y = y1;
        z = z1;
    }
};

struct NodeModel {
    QVector<CVector3<float> > v;
    QVector<CVector3<int> > f;
    QVector<CVector3<float> > vt;
};

class ObjReader
{
private:
    QFile *file;
    QHash<QString,NodeModel*> models;
public:
    ObjReader( QString name_files );
    ~ObjReader();

    void readFile();
    QVector<CVector3<float> > getV( QString model ) {
        return models[model]->v;
    }

    QVector<CVector3<int> > getF( QString model ) {
        return models[model]->f;
    }
};

#endif // OBJREADER_H
