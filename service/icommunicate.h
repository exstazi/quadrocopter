#ifndef ICOMMUNICATE
#define ICOMMUNICATE

#include <QObject>

class ICommunicate: public QObject
{
    Q_OBJECT
protected:
    QString status;
public:
    virtual bool communicate() = 0;
    QString getStatus() { return status; }
signals:
    void readyData( uchar command,int value );
};

#endif

