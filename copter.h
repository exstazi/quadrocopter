#ifndef COPTER_H
#define COPTER_H

#include <QObject>
#include <QTimer>
#include <iradioequimped.h>
#include <imemssensors.h>

#include "modelcopter.h"
#include "modeldefault.h"
#include "pid.h"

class Copter: public QObject
{
    Q_OBJECT
protected:
    IRadioEquimped * equimped;
    IMemsSensors * sensors;
    ModelCopter * model;

    float x;
    float y;
    float z;

    int roll;
    int pitch;
    int yaw;
    int gas;

    int front_l;
    int front_r;
    int rear_l;
    int rear_r;

    Pid pid_roll;
    Pid pid_pitch;
    Pid pid_yaw;

    QTimer timer;



public:
    Copter(IRadioEquimped * equimped);
    ~Copter();

    void setModel( ModelCopter *model );
    void print();
    void setSensors( IMemsSensors *sensors );

signals:
    void changedAngle( int roll, int pitch, int yaw );
    void changedRoll( int value );
    void changedPitch( int value );
    void changedPwm( int front_l,int front_r,int rear_l,int rear_r );
private slots:
    void changeGas( int value );
    void changeRoll( int value );
    void changePitch( int value );
    void changeYaw( int value );

    void update();
    void processingBarrier( int where );
};

#endif // COPTER_H
