#ifndef SCENEWORLD
#define SCENEWORLD

#include <QGLWidget>
#include <QList>
#include <QTimer>

#include "copter.h"

class SceneWorld: public QGLWidget
{
    Q_OBJECT
protected:
    QList<Copter *> copters;

    GLfloat rotate_x;
    GLfloat rotate_y;
    GLfloat rotate_z;
    GLfloat offset_x;
    GLfloat offset_y;
    GLfloat offset_z;
    GLfloat scale;

    QPoint ptrMousePosition;

    QTimer *timer;

    void initializeGL();
    void resizeGL(int nWidth, int nHeight );
    void wheelEvent(QWheelEvent* pe);
    void mouseMoveEvent(QMouseEvent* pe);
    void mousePressEvent(QMouseEvent* pe);
    //void keyPressEvent(QKeyEvent *pe);
public:
    explicit SceneWorld(QWidget *parent = 0);
    void addCopter( Copter *copter ) { copters.append( copter); }
    int count() { return copters.size(); }
    virtual float* checkPosistion( float x,float y,float z) = 0;
    virtual void setSizeWorld( uint length,uint width,uint height ) = 0;
    virtual void setFps( uint fps ) = 0;
};

#endif // SCENEWORLD

