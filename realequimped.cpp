#include "realequimped.h"
#include <QDebug>

RealEquimped::RealEquimped(ICommunicate *communicate)
{
    this->communicate = communicate;
    connect( communicate,SIGNAL(readyData(uchar,int)),this,SLOT(readData(uchar,int)) );
    communicate->communicate();
}

void RealEquimped::readData( uchar command, int value )
{
    qDebug() << command << " " << value;
    switch( command ) {
    case 0:
        emit changedStickRoll( value );
        break;
    case 1:
        emit changedStickPitch( value );
        break;
    case 2:
        emit changedStickGas( value );
        break;
    case 3:
        emit changedStickYaw( value );
        break;
    }
}

RealEquimped::~RealEquimped()
{
    delete communicate;
}

